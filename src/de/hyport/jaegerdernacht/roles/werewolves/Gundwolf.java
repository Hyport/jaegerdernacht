package de.hyport.jaegerdernacht.roles.werewolves;

import de.hyport.jaegerdernacht.roles.Werewolf;

public class Gundwolf extends Werewolf {

    private static final String ABILITY_TEXT =
            "Einmal im Spiel darfst du am Ende deiner Spielrunde bestimmen, dass dir bis zum Beginn deiner nächsten Runde " +
                    "kein Schaden zugefügt werden darf.";

    private boolean abilityActive;

    private final Ability ability = new Ability(ABILITY_TEXT) {

        @Override
        public void use() {
            reveal();
            abilityActive = true;
            disable();
        }
    };

    public Gundwolf() {
        super(14);
        abilityActive = false;
    }

    public Ability getAbility() {
        return ability;
    }

    public boolean isAbilityActive() {
        return abilityActive;
    }

    @Override
    public void startRound() {
        if(abilityActive) {
            abilityActive = false;
        }
    }

    @Override
    public void startEndRound() {
        ability.setReady(true);
    }

    @Override
    public void endRound() {
        ability.setReady(false);
    }
    @Override
    public String getName() {
        return "Gundwolf";
    }

}
