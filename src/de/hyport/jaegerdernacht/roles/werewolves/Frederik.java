package de.hyport.jaegerdernacht.roles.werewolves;

import de.hyport.jaegerdernacht.main.DamageCause;
import de.hyport.jaegerdernacht.main.DiceReason;
import de.hyport.jaegerdernacht.main.GamePlayer;
import de.hyport.jaegerdernacht.roles.Werewolf;

public class Frederik extends Werewolf {

    private static final String ABILITY_TEXT =
            "Einmal im Spiel darfst du zu Beginn deiner Spielrunde einen Spieler auswählen. Wirf den 6-seitigen Würfel" +
                    " und füge dem Spieler die gewürfelte Zahl an Schadenspunkten zu.";

    private final Ability ability = new Ability(ABILITY_TEXT) {
        @Override
        public void use() {
            reveal();
            GamePlayer target = getGameController().chooseTarget(getGamePlayer(), getName(), getText());
            if (target != null) {
                disable();
                target.increaseDamage(getGamePlayer().rollDiceSixer(DiceReason.ATTACK), DamageCause.SPECIAL, getGamePlayer());
            }
        }
    };

    public Frederik() {
        super(12);
    }

    public Ability getAbility() {
        return ability;
    }

    @Override
    public void startRound() {
        ability.setReady(true);
    }

    @Override
    public void startFieldChange() {
        ability.setReady(false);
    }

    @Override
    public String getName() {
        return "Frederik";
    }
}
