package de.hyport.jaegerdernacht.roles.werewolves;

import de.hyport.jaegerdernacht.main.GamePlayer;
import de.hyport.jaegerdernacht.roles.Werewolf;
import org.bukkit.Bukkit;

public class Etta extends Werewolf {

    private static final String ABILITY_TEXT =
            "Einmal im Spiel darfst du zu Beginn deiner Spielrunde einen Spieler bestimmen, dessen \"BESONDERHEIT\" bis zum Ende des Spiels nicht mehr gilt.";

    private final Ability ability = new Ability(ABILITY_TEXT) {
        @Override
        public void use() {
            reveal();
            GamePlayer target = getGameController().chooseTarget(getGamePlayer(), getName(), getText());
            if (target != null) {
                target.getRole().getAbility().disable();
                Bukkit.broadcastMessage(getGamePlayer().getPlayer().getDisplayName() + " hat die Fähigkeit von " + target.getPlayer().getDisplayName() + " blockiert!");
                disable();
            }
        }
    };

    public Etta() {
        super(10);
    }

    public Ability getAbility() {
        return ability;
    }

    @Override
    public void startRound() {
        ability.setReady(true);
    }

    @Override
    public void startFieldChange() {
        ability.setReady(false);
    }

    @Override
    public String getName() {
        return "Etta";
    }
}
