package de.hyport.jaegerdernacht.roles.werewolves;

import de.hyport.jaegerdernacht.main.DamageCause;
import de.hyport.jaegerdernacht.main.GamePlayer;
import de.hyport.jaegerdernacht.roles.Role;
import de.hyport.jaegerdernacht.roles.Werewolf;

public class Flicka extends Werewolf {

    private static final String ABILITY_TEXT =
            "Einmal im Spiel darfst du zu Beginn deiner Spielrunde den Marker eines beliebigen Spielers auf das Feld" +
                    " mit dem \"Blutmond\" versetzen.";

    private final Ability ability = new Ability(ABILITY_TEXT) {
        @Override
        public void use() {
            reveal();
            GamePlayer target = getGameController().chooseTargetFromList(getGamePlayer().getPlayer(), getGameController().getPlayers(),getName(), getText());
            if (target != null) {
                target.setDamage(7, DamageCause.SPECIAL, getGamePlayer());
                disable();
            }
        }
    };

    public Flicka() {
        super(12);
    }

    public Role.Ability getAbility() {
        return ability;
    }

    @Override
    public void startRound() {
        ability.setReady(true);
    }

    @Override
    public void startFieldChange() {
        ability.setReady(false);
    }

    @Override
    public String getName() {
        return "Flicka";
    }
}
