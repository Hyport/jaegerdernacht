package de.hyport.jaegerdernacht.roles.events;

import de.hyport.jaegerdernacht.main.GamePlayer;

public record AttackEvent(GamePlayer attacker, GamePlayer target, int damage) {

    public GamePlayer getAttacker() {
        return attacker;
    }

    public GamePlayer getTarget() {
        return target;
    }

    public int getDamage() {
        return damage;
    }
}
