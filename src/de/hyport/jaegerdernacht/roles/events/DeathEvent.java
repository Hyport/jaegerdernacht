package de.hyport.jaegerdernacht.roles.events;

import de.hyport.jaegerdernacht.main.GamePlayer;

public record DeathEvent(GamePlayer player) {

    public GamePlayer getPlayer() {
        return player;
    }
}
