package de.hyport.jaegerdernacht.roles.events;

public interface GameEventListener {

    default void receiveDeath(DeathEvent e) {}

    default void receiveAttack(AttackEvent e) {}
}
