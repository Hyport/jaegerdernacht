package de.hyport.jaegerdernacht.roles;

import de.hyport.jaegerdernacht.main.GamePlayer;
import org.bukkit.ChatColor;

public abstract class Werewolf extends Role {

    public Werewolf(int deadPoint) {
        super(RoleName.WEREWOLF, deadPoint);
    }

    @Override
    public boolean hasWon() {
        for(GamePlayer gp : getGamePlayer().getGameController().getPlayers()) {
            if(gp.getRole() instanceof Vampire && gp.isAlive()) {
                return false;
            }
        }
        return true;
    }

    @Override
    public String getObjective() {
        return "Alle " + ChatColor.RED + "Vampire" + ChatColor.RESET + " sind ausgeschieden!";
    }

    @Override
    public ChatColor getRoleColor() {
        return ChatColor.BLUE;
    }
}
