package de.hyport.jaegerdernacht.roles;

import org.bukkit.ChatColor;

public abstract class Human extends Role {

    public Human(int deadPoint) {
        super(RoleName.HUMAN, deadPoint);
    }

    @Override
    public ChatColor getRoleColor() {
        return ChatColor.YELLOW;
    }
}
