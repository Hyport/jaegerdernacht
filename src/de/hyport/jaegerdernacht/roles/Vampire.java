package de.hyport.jaegerdernacht.roles;

import de.hyport.jaegerdernacht.main.GamePlayer;
import org.bukkit.ChatColor;

public abstract class Vampire extends Role{

    public Vampire(int deadPoint) {
        super(RoleName.VAMPIRE, deadPoint);
    }

    @Override
    public boolean hasWon() {
        for(GamePlayer gp : getGamePlayer().getGameController().getPlayers()) {
            if(gp.getRole() instanceof Werewolf && gp.isAlive()) {
                return false;
            }
        }
        return true;
    }

    @Override
    public String getObjective() {
        return "Alle " + ChatColor.BLUE + "Werwölfe" + ChatColor.RESET + " sind ausgeschieden!";
    }

    @Override
    public ChatColor getRoleColor() {
        return ChatColor.RED;
    }
}
