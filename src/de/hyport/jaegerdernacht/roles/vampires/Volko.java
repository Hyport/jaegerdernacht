package de.hyport.jaegerdernacht.roles.vampires;

import de.hyport.jaegerdernacht.main.DamageCause;
import de.hyport.jaegerdernacht.main.GameController;
import de.hyport.jaegerdernacht.main.GamePlayer;
import de.hyport.jaegerdernacht.roles.Vampire;
import de.hyport.jaegerdernacht.roles.events.AttackEvent;
import de.hyport.jaegerdernacht.roles.events.GameEventListener;

public class Volko extends Vampire implements GameEventListener {

    private static final String ABILITY_TEXT = "Fügst du einem Spieler bei einem ANGRIFF Schadenspunkte zu, werden sofort 2 deiner eigenen Schadenspunkte geheilt.";

    private final Ability ability = new Ability(ABILITY_TEXT) {
        @Override
        public void use() {
            reveal();
            getGamePlayer().decreaseDamage(2, DamageCause.SPECIAL);
            setReady(false);
        }
    };

    public Volko() {
        super(13);
    }

    @Override
    public String getName() {
        return "Volko";
    }

    @Override
    public Ability getAbility() {
        return ability;
    }

    @Override
    public void receiveAttack(AttackEvent e) {
        if(e.getAttacker() == getGamePlayer() && e.getDamage() > 0) {
            if(ability.isEnabled() && isRevealed()) {
                ability.use();
            }else{
                ability.setReady(true);
            }
        }
    }

    @Override
    public void endRound() {
        ability.setReady(false);
    }
}
