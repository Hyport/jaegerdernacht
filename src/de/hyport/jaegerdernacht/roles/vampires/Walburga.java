package de.hyport.jaegerdernacht.roles.vampires;

import de.hyport.jaegerdernacht.main.GamePlayer;
import de.hyport.jaegerdernacht.roles.Vampire;
import de.hyport.jaegerdernacht.roles.events.AttackEvent;
import de.hyport.jaegerdernacht.roles.events.GameEventListener;

public class Walburga extends Vampire implements GameEventListener {

    private static final String ABILITY_TEXT =
            "Hat ein Spieler einen ANGRIFF auf dich durchgeführt, darfst du sofort danach einen ANGRIFF gegen ihn starten.";

    private GamePlayer lastAttacker;

    private final Ability ability = new Ability(ABILITY_TEXT) {
        @Override
        public void use() {
            reveal();
            setReady(false);
            int attackPoints = getGamePlayer().calculateAttackPoints();
            getGamePlayer().executeAttackOnPlayer(attackPoints, lastAttacker);
        }
    };

    public Walburga() {
        super(14);
    }

    @Override
    public String getName() {
        return "Walburga";
    }

    @Override
    public Ability getAbility() {
        return ability;
    }

    @Override
    public void receiveAttack(AttackEvent e) {
        if(e.getTarget() == getGamePlayer() && e.getDamage() > 0) {
            lastAttacker = e.getAttacker();
            ability.setReady(true);
        }
    }

    @Override
    public void startRound() {
        ability.setReady(false);
    }
}
