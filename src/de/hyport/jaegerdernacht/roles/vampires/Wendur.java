package de.hyport.jaegerdernacht.roles.vampires;

import de.hyport.jaegerdernacht.main.GamePlayer;
import de.hyport.jaegerdernacht.roles.Vampire;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;

public class Wendur extends Vampire {

    private static final String ABILITY_TEXT = "Einmal im Spiel bist du nach deiner Spielrunde weiter an der Reihe: Pro ausgeschiedenem Spieler hast du eine komplette Runde.";

    private int additionalRounds = 0;

    private final Ability ability = new Ability(ABILITY_TEXT) {
        @Override
        public void use() {
            reveal();
            for (GamePlayer gp : getGameController().getPlayers()) {
                if (!gp.isAlive()) {
                    additionalRounds++;
                }
            }
            Bukkit.broadcastMessage(getGamePlayer().getPlayer().getDisplayName() + " (" + getRoleColor() + getName() + ChatColor.RESET + ") ist nach diesem Zug noch " + ChatColor.YELLOW + additionalRounds + ChatColor.RESET + "-mal dran!");
            setReady(false);
            disable();
        }
    };


    public Wendur() {
        super(14);
    }

    public int getAdditionalRounds() {
        return additionalRounds > 0 ? additionalRounds-- : additionalRounds;
    }

    @Override
    public String getName() {
        return "Wendur";
    }

    @Override
    public Ability getAbility() {
        return ability;
    }

    @Override
    public void startEndRound() {
        ability.setReady(true);
    }

    @Override
    public void endRound() {
        ability.setReady(false);
    }
}
