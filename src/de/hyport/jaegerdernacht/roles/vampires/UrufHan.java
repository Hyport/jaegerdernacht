package de.hyport.jaegerdernacht.roles.vampires;

import de.hyport.jaegerdernacht.main.GamePlayer;
import de.hyport.jaegerdernacht.roles.Vampire;

public class UrufHan extends Vampire {

    private static final String ABILITY_TEXT =
            "Wenn du vom aktiven Spieler eine grüne Karte erhältst, darfst du bei der Antwort lügen. Du musst deine Charakterkarte nicht aufdecken.";

    private final Ability ability = new Ability(ABILITY_TEXT) {
        @Override
        public void use() {
            reveal();
        }
    };

    public UrufHan() {
        super(11);
    }

    @Override
    public void setGamePlayer(GamePlayer gamePlayer) {
        super.setGamePlayer(gamePlayer);
        ability.setReady(true);
    }

    @Override
    public String getName() {
        return "Uruf Han";
    }

    @Override
    public Ability getAbility() {
        return ability;
    }
}
