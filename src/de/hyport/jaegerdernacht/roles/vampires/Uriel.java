package de.hyport.jaegerdernacht.roles.vampires;

import de.hyport.jaegerdernacht.main.DamageCause;
import de.hyport.jaegerdernacht.main.Field;
import de.hyport.jaegerdernacht.main.GamePlayer;
import de.hyport.jaegerdernacht.roles.Vampire;

import java.util.LinkedList;

public class Uriel extends Vampire {

    private static final String ABILITY_TEXT =
            "Zu Beginn deiner Spielrunde darfst du einem Spieler, der sich auf dem \"Friedhof\" befindet, 3 Schadenspunkte zufügen.";

    private final Ability ability = new Ability(ABILITY_TEXT) {
        @Override
        public void use() {
            reveal();
            LinkedList<GamePlayer> playersFriedhof = getGameController().getField(Field.FRIEDHOF.getName()).getPlayers(getGameController().getPlayers());
            if (playersFriedhof.size() > 0) {
                GamePlayer target = getGameController()
                        .chooseTargetFromList(
                                getGamePlayer().getPlayer(),
                                playersFriedhof,
                                "",
                                getText());
                if (target != null) {
                    target.increaseDamage(3, DamageCause.SPECIAL, getGamePlayer());
                    setReady(false);
                }
            }
        }
    };

    public Uriel() {
        super(11);
    }

    @Override
    public String getName() {
        return "Uriel";
    }

    @Override
    public Ability getAbility() {
        return ability;
    }

    @Override
    public void startRound() {
        ability.setReady(true);
    }

    @Override
    public void startFieldChange() {
        ability.setReady(false);
    }
}
