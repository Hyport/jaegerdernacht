package de.hyport.jaegerdernacht.roles.vampires;

import de.hyport.jaegerdernacht.main.GameController;
import de.hyport.jaegerdernacht.roles.Vampire;

public class Valkyra extends Vampire {

    private static final String ABILITY_TEXT =
            "Deinen Angriff musst du mit dem 4-seitigen Würfel ausführen. Du verursachst so viele Schadenspunkte, wie die gewürfelte Zahl anzeigt.";

    private final Ability ability = new Ability(ABILITY_TEXT) {
        @Override
        public void use() {
            reveal();
            setReady(false);
        }
    };

    public Valkyra() {
        super(13);
    }

    @Override
    public String getName() {
        return "Valkyra";
    }

    @Override
    public Ability getAbility() {
        return ability;
    }

    @Override
    public void startFieldChange() {
        if(!isRevealed())
            ability.setReady(true);
    }

    @Override
    public void startAttack() {
        ability.setReady(false);
    }
}
