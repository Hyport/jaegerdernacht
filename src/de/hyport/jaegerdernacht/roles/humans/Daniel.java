package de.hyport.jaegerdernacht.roles.humans;

import de.hyport.jaegerdernacht.main.GamePlayer;
import de.hyport.jaegerdernacht.roles.Human;
import de.hyport.jaegerdernacht.roles.Vampire;
import de.hyport.jaegerdernacht.roles.events.DeathEvent;
import de.hyport.jaegerdernacht.roles.events.GameEventListener;
import org.bukkit.ChatColor;


public class Daniel extends Human implements GameEventListener {

    private static final String ABILITY_TEXT = "Sobald ein Spieler ausscheidet, musst du deine Charakterkarte aufdecken.";

    private final Ability ability = new Ability(ABILITY_TEXT) {
        @Override
        public void use() {
            reveal();
            disable();
        }
    };

    public Daniel() {
        super(13);
    }

    @Override
    public String getObjective() {
        return "Du scheidest als erster Spieler aus! Oder: Alle " + ChatColor.RED + "Vampire" + ChatColor.RESET + " sind ausgeschieden und du bist noch im Spiel!";
    }

    @Override
    public String getName() {
        return "Daniel";
    }

    @Override
    public Ability getAbility() {
        return ability;
    }

    @Override
    public boolean hasWon() {
        return onlyDeath() || allVampiresDead();
    }

    private boolean allVampiresDead() {
        for(GamePlayer gp : getGameController().getPlayers()) {
            if(gp.getRole() instanceof Vampire && gp.isAlive()) {
                return false;
            }
        }
        return true;
    }

    private boolean onlyDeath() {
        for(GamePlayer gp : getGameController().getPlayers()) {
            if(!gp.isAlive() && gp != getGamePlayer()) {
                return false;
            }
        }
        return !getGamePlayer().isAlive();
    }

    // Listener
    @Override
    public void receiveDeath(DeathEvent e) {
        ability.use();
    }
}
