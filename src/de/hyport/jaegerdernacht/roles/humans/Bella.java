package de.hyport.jaegerdernacht.roles.humans;

import de.hyport.jaegerdernacht.main.Card;
import de.hyport.jaegerdernacht.main.GamePlayer;
import de.hyport.jaegerdernacht.roles.Human;
import de.hyport.jaegerdernacht.roles.events.AttackEvent;
import de.hyport.jaegerdernacht.roles.events.GameEventListener;


public class Bella extends Human implements GameEventListener {

    private static final String ABILITY_TEXT = "Wenn ein Spieler durch deinen ANGRIFF ausscheidet, darfst du alle seine Ausrüstungskarten nehmen.";
    private GamePlayer attackTarget;

    private final Ability ability = new Ability(ABILITY_TEXT) {
        @Override
        public void use() {
            reveal();
            for (Card c : attackTarget.getInventory()) {
                getGamePlayer().getInventory().add(c);
            }
        }
    };

    public Bella() {
        super(10);
    }

    @Override
    public String getObjective() {
        return "Bringe 4 oder mehr Ausrüstungskarten in deinen Besitz!";
    }

    @Override
    public String getName() {
        return "Bella";
    }

    @Override
    public Ability getAbility() {
        return ability;
    }

    @Override
    public boolean hasWon() {
        int count = 0;
        for (Card c : getGamePlayer().getInventory()) {
            if (c.getType().equalsIgnoreCase("Ausrüstung")) {
                count ++;
            }
        }
        return count >= 4;
    }

    // Listener

    @Override
    public void receiveAttack(AttackEvent e) {
        if (!e.getTarget().isAlive() && e.getAttacker() == getGamePlayer()) {
            attackTarget = e.getTarget();
            ability.setReady(true);
        }
    }

    @Override
    public void startRound() {
        ability.setReady(false);
        attackTarget = null;
    }
}
