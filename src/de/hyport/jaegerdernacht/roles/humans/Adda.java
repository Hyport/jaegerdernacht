package de.hyport.jaegerdernacht.roles.humans;

import de.hyport.jaegerdernacht.main.DamageCause;
import de.hyport.jaegerdernacht.main.GamePlayer;
import de.hyport.jaegerdernacht.roles.Human;
import org.bukkit.ChatColor;


public class Adda extends Human {

    private static final String ABILITY_TEXT = "Einmal im Spiel kannst du alle deine Schadenspunkte heilen.";

    private final Ability ability = new Ability(ABILITY_TEXT) {
        @Override
        public void use() {
            reveal();
            getGamePlayer().setDamage(0, DamageCause.SPECIAL, getGamePlayer());
            disable();
        }
    };

    public Adda() {
        super(8);
    }

    @Override
    public void setGamePlayer(GamePlayer gamePlayer) {
        super.setGamePlayer(gamePlayer);
        ability.setReady(true);
    }

    @Override
    public String getObjective() {
        return "Du bist " + ChatColor.BOLD + "nicht" + ChatColor.RESET + " ausgeschieden, wenn das Spiel endet!";
    }

    @Override
    public String getName() {
        return "Adda";
    }

    @Override
    public Ability getAbility() {
        return ability;
    }

    @Override
    public boolean hasWon() {
        return !getGameController().isGameRunning() && getGamePlayer().isAlive();
    }

}
