package de.hyport.jaegerdernacht.roles.humans;

import de.hyport.jaegerdernacht.main.DamageCause;
import de.hyport.jaegerdernacht.main.GamePlayer;
import de.hyport.jaegerdernacht.roles.Human;

public class Chloe extends Human {

    private static final String ABILITY_TEXT = "Zu Beginn deiner Spielrunde darfst du 1 deiner eigenen Schadenspunkte heilen.";

    private final Ability ability = new Ability(ABILITY_TEXT) {
        @Override
        public void use() {
            reveal();
            getGamePlayer().decreaseDamage(1, DamageCause.SPECIAL);
            setReady(false);
        }
    };

    public Chloe() {
        super(11);
    }

    @Override
    public String getName() {
        return "Chloe";
    }

    @Override
    public Ability getAbility() {
        return ability;
    }

    @Override
    public String getObjective() {
        return "Du scheidest als erster Spieler aus! Oder: Du bist einer der beiden letzten Spieler im Spiel!";
    }

    @Override
    public boolean hasWon() {
        return onlyDeath() || getGamePlayer().isAlive() && numberAlivePlayers() <= 2;
    }

    private int numberAlivePlayers() {
        int n = 0;
        for (GamePlayer gp : getGameController().getPlayers()) {
            if (gp.isAlive()) {
                n++;
            }
        }
        return n;
    }

    private boolean onlyDeath() {
        for(GamePlayer gp : getGameController().getPlayers()) {
            if(!gp.isAlive() && gp != getGamePlayer()) {
                return false;
            }
        }
        return !getGamePlayer().isAlive();
    }

    @Override
    public void startRound() {
        ability.setReady(true);
    }

    @Override
    public void startFieldChange() {
        ability.setReady(false);
    }
}
