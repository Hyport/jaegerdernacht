package de.hyport.jaegerdernacht.roles;

import de.hyport.jaegerdernacht.main.GameController;
import de.hyport.jaegerdernacht.main.GamePlayer;
import de.hyport.jaegerdernacht.ui.ClickableItems;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;


public abstract class Role {

    public enum RoleName {
        WEREWOLF("Werwolf"), VAMPIRE("Vampir"), HUMAN("Mensch");

        private final String roleName;
        RoleName(String roleName) {
            this.roleName = roleName;
        }

        @Override
        public String toString() {
            return roleName;
        }
    }

    public abstract class Ability {
        private boolean enabled = true;
        private boolean ready = false;
        private final String text;

        public Ability(String text) {
            this.text = text;
        }

        public void disable() {
            enabled = false;
            setReady(false);
        }

        public void setReady(boolean ready) {
            if (ready) {
                ItemStack abilityItem;
                if (enabled) {
                    abilityItem = ClickableItems.ITEM_ABILITY.getItem("Fähigkeit", getAbility().getText());
                } else {
                    abilityItem = new ItemStack(ClickableItems.ITEM_ABILITY.getMaterial());
                    ItemMeta meta = abilityItem.getItemMeta();
                    assert meta != null;
                    meta.setDisplayName(ChatColor.DARK_GRAY + "[Nicht verfügbar]");
                    abilityItem.setItemMeta(meta);
                }
                Bukkit.getScheduler().runTask(getGameController().getPlugin(), () -> {
                    gamePlayer.getPlayer().getInventory().setItem(7, abilityItem);
                });
            } else {
                Bukkit.getScheduler().runTask(getGameController().getPlugin(), () -> {
                    gamePlayer.getPlayer().getInventory().setItem(7, new ItemStack(Material.AIR));
                });
            }
            this.ready = ready;
        }

        public boolean isEnabled() {
            return enabled;
        }

        public boolean isReady() {
            return ready;
        }

        public String getText() {
            return text;
        }

        public abstract void use();
    }

    private final String roleName;
    private final int deadPoint;
    private boolean revealed = false;
    private GamePlayer gamePlayer;

    public Role(RoleName roleName, int deadPoint) {
        this.roleName = roleName.toString();
        this.deadPoint = deadPoint;
    }

    public void setGamePlayer(GamePlayer gamePlayer) {
        this.gamePlayer = gamePlayer;
    }

    public String getRoleName() {
        return roleName;
    }

    public int getDeadPoint() {
        return deadPoint;
    }

    public void reveal() {
        if(!revealed) {
            getAbility().setReady(false);
            Bukkit.broadcastMessage(gamePlayer.getPlayer().getDisplayName() + " deckt die Charakterkarte auf. Identität: " + getRoleColor() + getName() + " (" + getRoleName() + ")");
            revealed = true;
        }
    }

    public boolean isRevealed() {
        return revealed;
    }

    public abstract ChatColor getRoleColor();

    public abstract String getName();

    public abstract Ability getAbility();

    public abstract String getObjective();

    public GamePlayer getGamePlayer() {
        return gamePlayer;
    }

    public abstract boolean hasWon();

    protected GameController getGameController() {
        return gamePlayer.getGameController();
    }

    // Game schedule
    public void startRound() {}
    public void startFieldChange() {}
    public void startAttack() {}
    public void startEndRound() {}
    public void endRound() {}
}
