package de.hyport.jaegerdernacht.worldmanagement;

import de.hyport.jaegerdernacht.main.Field;
import de.hyport.jaegerdernacht.main.GameController;
import de.hyport.jaegerdernacht.main.GamePlayer;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.*;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.player.*;

public class BasicListener implements Listener {

    private final GameController gameController;

    public BasicListener(GameController gameController) {
        this.gameController = gameController;
    }


    @EventHandler
    public void onBuild(BlockPlaceEvent e) {
        if (gameController.isGameRunning() || !e.getPlayer().getGameMode().equals(GameMode.CREATIVE)) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onDestroy(BlockBreakEvent e) {
        if (gameController.isGameRunning() || !e.getPlayer().getGameMode().equals(GameMode.CREATIVE)) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent e) {
        Player p = e.getPlayer();
        p.setGameMode(GameMode.ADVENTURE);
        p.getInventory().clear();
        String name = p.getName();
        p.setDisplayName(getNickname(name));
        p.teleport(Field.MITTE.getLocation());
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent e) {
        Player p = e.getPlayer();
        gameController.getGamePlayer(p).die(gameController.getGamePlayer(p));
        gameController.checkIfGameEnds();
    }

    public static String getNickname(String playerName) {
        String nickname = playerName;
        switch (playerName) {
            case "BenLuno" -> nickname = "Luno";
            case "Jaulhausener" -> nickname = "Henny";
            case "_WaterLily" -> nickname = "Lily";
            case "Lycheemus", "Juko007" -> nickname = "Julius";
            case "Smartpel" -> nickname = "Jakob";
            case "Coltfrau3" -> nickname = "Lea";
            case "Kammingham" -> nickname = "Fabi";
            case "PreDatorEye" -> nickname = "Mattis";
        }
        return nickname;
    }

    @EventHandler
    public void onRespawn(PlayerRespawnEvent e) {
        Player p = e.getPlayer();
        GamePlayer gp = gameController.getGamePlayer(p);
        if (gp != null) {
            e.setRespawnLocation(gp.getField().getLocation());
        }
    }

     @EventHandler
    public void onDamage(EntityDamageEvent e) {
        if(e.getEntity() instanceof Player p) {
            if (e.getCause().equals(EntityDamageEvent.DamageCause.VOID)) {
                Bukkit.broadcastMessage(p.getDisplayName() + " hat versucht seinem/ihrem Schicksal zu entrinnen.");
                GamePlayer gp = gameController.getGamePlayer(p);
                if (gp != null) {
                    p.teleport(gp.getField().getLocation());
                } else {
                    p.teleport(Field.MITTE.getLocation());
                }
            }
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onRecipe(PlayerRecipeDiscoverEvent e) {
        e.setCancelled(true);
    }

    @EventHandler
    public void onItemDrop(PlayerDropItemEvent e) {
        if(gameController.isGameRunning()) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onFoodLevelChange(FoodLevelChangeEvent e) {
        if (gameController.isGameRunning()) e.setCancelled(true);
    }


    @EventHandler
    public void noUproot(PlayerInteractEvent event)
    {
        if(event.getAction() == Action.PHYSICAL && event.getClickedBlock().getType() == Material.FARMLAND)
            event.setCancelled(true);
    }


    @EventHandler
    public void onGrow(BlockGrowEvent e) {
        e.setCancelled(true);
    }

}
