package de.hyport.jaegerdernacht.ui;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;

import java.util.Arrays;

public class SkullBuilder extends ItemBuilder{


    // creates skull
    public SkullBuilder(Player player) {
        ItemStack item = new ItemStack(Material.PLAYER_HEAD);
        SkullMeta meta = (SkullMeta) item.getItemMeta();
        assert meta != null;
        meta.setOwningPlayer(Bukkit.getOfflinePlayer(player.getUniqueId()));
        meta.setDisplayName(player.getDisplayName());
        setItem(item);
        setItemMeta(meta);
    }

    @Override
    public SkullBuilder setLore(String... lore) {
        getItemMeta().setLore(Arrays.asList(lore));
        return this;
    }
}
