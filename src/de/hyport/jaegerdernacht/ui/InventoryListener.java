package de.hyport.jaegerdernacht.ui;

import de.hyport.jaegerdernacht.main.*;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.CraftingInventory;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.Objects;
import java.util.concurrent.atomic.AtomicBoolean;

public class InventoryListener implements Listener {

    public static final String KARTEN = "§dKarten";
    public static final String ORT_WAHL = "Wähle einen Ort aus:";
    public static final String FARB_WAHL = "Wähle deine Farbe:";
    public static final String KARTEN_WAHL = "Wähle eine Karte aus:";
    public static final String SPIELER_WAHL = "Wähle einen Spieler:";
    public static final String ZIEHE_KARTE = "Ziehe Karte ...";
    public static final String GRUENE_KARTE = "Wähle Konsequenz der grünen Karte:";

    public static AtomicBoolean quellChoiceMade = new AtomicBoolean(true);

    private final GameController gameController;

    public InventoryListener(GameController gameController) {
        this.gameController = gameController;
    }

    @EventHandler
    public void onClickItem(InventoryClickEvent e) {
        ItemStack clickedItem = e.getCurrentItem();
        if (clickedItem != null && clickedItem.hasItemMeta()) {
            assert clickedItem.getItemMeta() != null;
            Player p = (Player) e.getWhoClicked();
            String displayName = clickedItem.getItemMeta().getDisplayName();
            String inventoryTitle = e.getView().getTitle();
            if (inventoryTitle.equals(GRUENE_KARTE)) {
                if (displayName.equalsIgnoreCase(ClickableItems.ITEM_NO.getColor() + "Schaden")) {
                    gameController.getGamePlayer(p).increaseDamage(1, DamageCause.SPECIAL, gameController.getGamePlayer(p));
                    GameController.userAnswer.set("Done");
                } else if (displayName.equalsIgnoreCase(ClickableItems.ITEM_YES.getColor() + "Heilung")) {
                    gameController.getGamePlayer(p).decreaseDamage(1, DamageCause.SPECIAL);
                    GameController.userAnswer.set("Done");
                } else if (displayName.equalsIgnoreCase(ClickableItems.ITEM_DRAW_CARD.getColor() + "Karte geben")) {
                    Bukkit.getScheduler().runTaskAsynchronously(gameController.getPlugin(), () -> {
                        GamePlayer source = Field.GREEN_CARD_SOURCE.get();
                        Card c = gameController.chooseCardFromInventory(source.getPlayer(), gameController.getGamePlayer(p).getInventory());
                        if (c != null) {
                            Bukkit.broadcastMessage(p.getDisplayName() + " gibt " + c.getChatColor() + c.getTitle() + ChatColor.RESET + " an " + source.getPlayer().getDisplayName() + "!");
                            source.getInventory().add(c);
                            gameController.getGamePlayer(p).getInventory().remove(c);
                        }
                        Bukkit.getScheduler().runTask(gameController.getPlugin(), p::closeInventory);
                        GameController.userAnswer.set("Done");
                    });
                } else if (displayName.equalsIgnoreCase(ClickableItems.ITEM_SHOW_IDENTITY.getColor() + "Zeige Identität")) {
                    GamePlayer source = Field.GREEN_CARD_SOURCE.get();
                    GamePlayer target = gameController.getGamePlayer(p);
                    Bukkit.broadcastMessage(source.getPlayer().getDisplayName() + " sieht jetzt die Karte von " + target.getPlayer().getDisplayName() + "!");
                    source.getPlayer().sendMessage("");
                    source.getPlayer().sendMessage("Identität von " + p.getDisplayName() + ":");
                    handleRoleInfo(target, target.getRole().getRoleColor() + target.getRole().getName(), source.getPlayer());
                    GameController.userAnswer.set("Done");
                } else if (displayName.equalsIgnoreCase(ClickableItems.ITEM_QUIT.getColor() + "Nichts geschieht")) {
                    Bukkit.broadcastMessage("Nichts geschieht...");
                    GameController.userAnswer.set("Done");
                }
            } else if ((inventoryTitle.equals(ORT_WAHL) || inventoryTitle.equals(FARB_WAHL) || inventoryTitle.equals(SPIELER_WAHL) || inventoryTitle.equals(KARTEN_WAHL)
                    || e.getInventory().contains(Material.END_CRYSTAL)) // Boolean Frage
                    && e.getSlot() > 8                                  // keines der Items im Schnellzugriff
                    && !clickedItem.getType().equals(Material.END_CRYSTAL)) {
                GameController.userAnswer.set(displayName);
            } else if (inventoryTitle.equalsIgnoreCase(ZIEHE_KARTE)) {
                if (displayName.equalsIgnoreCase("§2Weiter:")) {
                    p.playSound(p.getLocation(), Sound.UI_BUTTON_CLICK, 1, 1);
                    GameController.userAnswer.set("Done");
                }
            } else if (inventoryTitle.equalsIgnoreCase("Quell der Weisheit:")) {
                GameController.userAnswer.set(displayName);
                quellChoiceMade.set(true);
            } else if(displayName.contains("Karte geben")){ // pass green card to player
                GameController.userAnswer.set("Done");
            } else if(inventoryTitle.equalsIgnoreCase("Spielerübersicht:")) {
                if(e.getClick().equals(ClickType.LEFT)) {
                    if(gameController.getPlayers().contains(gameController.getPlayerByName(displayName))) {
                        UserAnswerHandler.increaseCloseInventoryTokens(p);
                        new InventoryBuilder(p, "Kartenübersicht").seeAllCards(gameController.getPlayerByName(displayName)).build(gameController.getPlugin());
                    }
                } else if(e.getClick().equals(ClickType.RIGHT)) {
                    if(gameController.getPlayers().contains(gameController.getPlayerByName(displayName))) {
                        UserAnswerHandler.increaseCloseInventoryTokens(p);
                        new InventoryBuilder(p, "Vermutung wählen:").createChooseGuessInventory(gameController.getGamePlayer(p), gameController.getPlayerByName(displayName)).build(gameController.getPlugin());
                    }
                }
            } else if(inventoryTitle.equalsIgnoreCase("Vermutung wählen:")) {
                GamePlayer chooseRolePlayer = gameController.getPlayerByName(e.getInventory().getItem(4).getItemMeta().getDisplayName());
                GamePlayer inventoryOwner = gameController.getGamePlayer(p);
                if (displayName.equalsIgnoreCase("§cVampir")) {
                    inventoryOwner.getGuesses().remove(chooseRolePlayer);
                    inventoryOwner.getGuesses().put(chooseRolePlayer, "Vampir");
                    UserAnswerHandler.increaseCloseInventoryTokens(p);
                    new InventoryBuilder(p, "Spielerübersicht:").createPlayerInventar(gameController, gameController.getGamePlayer(p)).build(gameController.getPlugin());
                } else if (displayName.equalsIgnoreCase("§9Werwolf")) {
                    inventoryOwner.getGuesses().remove(chooseRolePlayer);
                    inventoryOwner.getGuesses().put(chooseRolePlayer, "Werwolf");
                    UserAnswerHandler.increaseCloseInventoryTokens(p);
                    new InventoryBuilder(p, "Spielerübersicht:").createPlayerInventar(gameController, gameController.getGamePlayer(p)).build(gameController.getPlugin());
                } else if (displayName.equalsIgnoreCase("§eMensch")) {
                    inventoryOwner.getGuesses().remove(chooseRolePlayer);
                    inventoryOwner.getGuesses().put(chooseRolePlayer, "Mensch");
                    UserAnswerHandler.increaseCloseInventoryTokens(p);
                    new InventoryBuilder(p, "Spielerübersicht:").createPlayerInventar(gameController, gameController.getGamePlayer(p)).build(gameController.getPlugin());
                }
            }
        }
        if(gameController.isGameRunning()) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onInventoryClose(InventoryCloseEvent e) {
        if (e.getInventory() instanceof CraftingInventory) return;
        if (!UserAnswerHandler.hasCloseInventoryTokens((Player) e.getPlayer())) {
        //if (GameController.waitingForPlayer.get().equals(e.getPlayer().getName())) {
            new BukkitRunnable() {
                @Override
                public void run() {
                    e.getPlayer().openInventory(e.getInventory());
                }
            }.runTaskLater(gameController.getPlugin(), 1);
        } else {
            UserAnswerHandler.decreaseCloseInventoryTokens((Player) e.getPlayer());
        }
        /*if(inventoryTitle.equalsIgnoreCase(ZIEHE_KARTE)) {
            if(!e.getInventory().getItem(26).getType().equals(Material.ARROW)) {
                GameController.userAnswer.compareAndSet("", getRandomItemName(e.getInventory()));
            } else {
                CardAnimation.endScreenOpen.set(false);
                GameController.userAnswer.compareAndSet("", "Done");
            }
        } else if(inventoryTitle.equalsIgnoreCase("Quell der Weisheit:") && !quellChoiceMade.get()) {
            String[] possibleAnswers = {"green", "blue", "red"};
            GameController.userAnswer.set(possibleAnswers[(int)(Math.random()*3)]);
            quellChoiceMade.set(true);
        }*/
    }

    private String getRandomItemName(Inventory inv) {
        for (ItemStack item : inv.getContents()) {
            if (item != null) {
                if (item.hasItemMeta() && !item.getType().equals(Material.END_CRYSTAL)) {
                    ItemMeta meta = item.getItemMeta();
                    assert meta != null;
                    return meta.getDisplayName();
                }
            }
        }
        return "null";
    }

    /**
     * Handles all items that are clicked while in main hand
     */
    @EventHandler
    public void onInteract(PlayerInteractEvent e) {
        Player p = e.getPlayer();
        ItemStack currentItem = e.getItem();
        if (e.getAction().equals(Action.RIGHT_CLICK_AIR) || e.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {
            if (currentItem != null && gameController.isGameRunning()) {
                GamePlayer gp = gameController.getGamePlayer(p);
                if (gp == null) return;
                String displayName = Objects.requireNonNull(currentItem.getItemMeta()).getDisplayName();
                if (displayName.equals(KARTEN)) {
                    UserAnswerHandler.increaseCloseInventoryTokens(p);
                    gameController.showInventory(p, gameController.getGamePlayer(p).getInventory());
                }
                else if (displayName.equals("§bWürfel")) {
                    GameController.diceFieldChangeReady.set(true);
                }
                else if (displayName.equals(gp.getRole().getRoleColor() + gp.getRole().getName())) {
                    handleRoleInfo(gp, displayName, gp.getPlayer());
                }
                else if (displayName.equals(ClickableItems.ITEM_ABILITY.getColor() + "Fähigkeit")) {
                    Bukkit.getScheduler().runTaskAsynchronously(gameController.getPlugin(), () -> gp.getRole().getAbility().use());
                }
                else if (displayName.equals(ClickableItems.END_ROUND.getColor() + "WEITER")) {
                    handleContinue(p);
                }
                else if (displayName.contains("Magischer Kompass")) {
                    p.getInventory().remove(Material.COMPASS);
                    p.getInventory().remove(Material.DIAMOND);
                    GameController.diceResult.set(1);
                    GameController.diceFieldChangeReady.set(true);
                }
                else if (displayName.equalsIgnoreCase("§bSpielerübersicht")) {
                    UserAnswerHandler.increaseCloseInventoryTokens(p);
                    new InventoryBuilder(p, "Spielerübersicht:").createPlayerInventar(gameController, gameController.getGamePlayer(p)).build(gameController.getPlugin());
                }
                e.setCancelled(true);
            }
        }
    }

    public static void handleDice(Player p, GameController gameController) {
        // increase boss bar progress
        if (GameController.diceReason.get() == DiceReason.FIELD_CHANGE) {
            gameController.getBossBar().setProgress(0.2);
        }
        // get number randomly (throw virtual dices)
        int number = (int)(Math.random()*6)+1 + (int)(Math.random()*4)+1;
        if (number != 7) {
            if (Field.getField(number).equals(gameController.getGamePlayer(p).getField())) {
                handleDice(p, gameController);
                return;
            }
        }
        // dice animation
        TitleBuilder titleBuilder = new TitleBuilder(gameController.getPlugin(), p);
        long duration = titleBuilder.rollDiceAnimation(number, 10, "", ChatColor.GREEN, 0);
        // show result and trigger teleportation
        p.getInventory().setItem(0, new ItemStack(Material.AIR));
        p.getInventory().setItem(2, new ItemStack(Material.AIR));
        new BukkitRunnable() {
            @Override
            public void run() {
                for (Player pl : Bukkit.getOnlinePlayers()) {
                    pl.sendMessage(p.getDisplayName() + " hat eine §a" + number + "§r gewürfelt.");
                }
                p.playSound(p.getLocation(), Sound.BLOCK_PORTAL_TRIGGER, 1, 1);
            }
        }.runTaskLater(gameController.getPlugin(), duration-60);
        if (GameController.diceReason.get() == DiceReason.FIELD_CHANGE) {
            new BukkitRunnable() {
                @Override
                public void run() {
                    GameController.diceResult.set(number);
                    GameController.diceDone.set(true);
                }
            }.runTaskLater(gameController.getPlugin(), duration + 5);

        }
    }

    private void handleRoleInfo(GamePlayer gp, String displayName, Player p) {
        p.sendMessage(ChatColor.DARK_GRAY+"--------------------------------------");
        p.sendMessage(ChatColor.GRAY + "Name: " + displayName);
        p.sendMessage(ChatColor.GRAY + "Team: " + gp.getRole().getRoleColor()+gp.getRole().getRoleName());
        p.sendMessage(ChatColor.GRAY+ "Stirbt bei " + ChatColor.YELLOW + gp.getRole().getDeadPoint() + ChatColor.GRAY + " Schaden.");
        p.sendMessage(ChatColor.DARK_GRAY+"--------------------------------------");
        p.sendMessage(ChatColor.GREEN + "" + ChatColor.BOLD + "Ziel:");
        String[] objectiveText = gp.getRole().getObjective().split("\\s");
        for (int i = 0; i < objectiveText.length; i+=4) {
            String s = ChatColor.GRAY + objectiveText[i];
            if(objectiveText.length > i+1)
                s += " " + ChatColor.GRAY + objectiveText[i+1];
            if(objectiveText.length > i+2)
                s += " " + ChatColor.GRAY + objectiveText[i+2];
            if (objectiveText.length > i+3)
                s += " " + ChatColor.GRAY + objectiveText[i+3];
            p.sendMessage(ChatColor.GRAY + s);
        }
        p.sendMessage(ChatColor.DARK_GRAY+"--------------------------------------");
        p.sendMessage(ChatColor.GREEN + "" + ChatColor.BOLD + "Fähigkeit:");
        String abilityText = gp.getRole().getAbility().getText();
        String[] splitAbilityText = abilityText.split("\\s");
        for (int i = 0; i < splitAbilityText.length; i+=4) {
            String s = ChatColor.GRAY + splitAbilityText[i];
            if(splitAbilityText.length > i+1)
                s += " " + ChatColor.GRAY + splitAbilityText[i+1];
            if(splitAbilityText.length > i+2)
                s += " " + ChatColor.GRAY + splitAbilityText[i+2];
            if (splitAbilityText.length > i+3)
                s += " " + ChatColor.GRAY + splitAbilityText[i+3];
            p.sendMessage(ChatColor.GRAY + s);
        }
        p.sendMessage(ChatColor.DARK_GRAY+"--------------------------------------");
    }

    private void handleContinue(Player p) {
        p.getInventory().remove(ClickableItems.END_ROUND.getMaterial());
        Bukkit.getScheduler().runTaskAsynchronously(gameController.getPlugin(), () -> {
            while (!GameController.diceDone.get()) {
                try {
                    Thread.sleep(100);
                } catch (InterruptedException ignored) {
                }
            }
            Bukkit.getScheduler().runTask(gameController.getPlugin(), () -> p.playSound(p.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1, 1));
            GameController.userAnswer.set("Continue");
        });
    }
}
