package de.hyport.jaegerdernacht.ui;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.Arrays;

public class ItemBuilder {
    private ItemStack item;
    private ItemMeta itemMeta;

    @SuppressWarnings("deprecation")
    public ItemBuilder(Material material, short subID) {
        item = new ItemStack(material, 1, subID);
        itemMeta = item.getItemMeta();
    }

    public ItemBuilder(Material material) {
        this(material, (short)0);
    }

    protected ItemBuilder() {

    }

    protected void setItem(ItemStack item) {
        this.item = item;
    }

    protected ItemStack getItem() {
        return item;
    }

    protected void setItemMeta(ItemMeta meta) {
        this.itemMeta = meta;
    }

    protected ItemMeta getItemMeta() {
        return itemMeta;
    }

    public ItemBuilder setName(String name) {
        itemMeta.setDisplayName(name);
        return this;
    }

    public ItemBuilder setAmount(int amount) {
        item.setAmount(amount);
        return this;
    }

    public ItemBuilder setEnchant(Enchantment enchant) {
        itemMeta.addEnchant(enchant, 1, false);
        return this;
    }

    public ItemBuilder setLocalizedName(String name) {
        itemMeta.setLocalizedName(name);
        return this;
    }

    public ItemBuilder setLore(String... lore) {
        itemMeta.setLore(Arrays.asList(lore));
        return this;
    }
    public ItemBuilder setLore(ArrayList<String> text) {
        itemMeta.setLore(text);
        return this;
    }
    public ItemStack build() {
        item.setItemMeta(itemMeta);
        return item;
    }

}
