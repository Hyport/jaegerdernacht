package de.hyport.jaegerdernacht.ui;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;

public enum ClickableItems {

    ARMBRUST(Material.CROSSBOW, ChatColor.RED),
    FACKEL(Material.TORCH, ChatColor.RED),
    EISENFAUST(Material.RAW_IRON, ChatColor.RED),
    FEUERZAUBER(Material.BLAZE_POWDER, ChatColor.RED),
    MAGISCHER_KOMPASS(Material.COMPASS, ChatColor.AQUA),
    KNOCHENLANZE(Material.TRIDENT, ChatColor.AQUA),
    TALISMAN(Material.GOLDEN_CARROT, ChatColor.AQUA),
    SCHUTZAMULETT(Material.EMERALD, ChatColor.AQUA),
    SCHUTZRING(Material.MAGMA_CREAM, ChatColor.AQUA),
    SCHUTZUMHANG(Material.ELYTRA, ChatColor.AQUA),
    RUCKSACK(Material.SADDLE, ChatColor.AQUA),
    RED_CARD(Material.RED_BANNER, ChatColor.RED),
    BLUE_CARD(Material.BLUE_BANNER, ChatColor.AQUA),
    GREEN_CARD(Material.GREEN_BANNER, ChatColor.GREEN),
    ITEM_QUESTION(Material.END_CRYSTAL, ChatColor.AQUA),
    ITEM_YES(Material.GREEN_WOOL, ChatColor.GREEN),
    ITEM_NO(Material.RED_WOOL, ChatColor.RED),
    ITEM_DRAW_CARD(Material.SPECTRAL_ARROW, ChatColor.RED),
    OXANAS_HUETTE(Material.MUSIC_DISC_CAT, ChatColor.DARK_GREEN),
    QUELL_DER_WEISHEIT(Material.MUSIC_DISC_MELLOHI, ChatColor.LIGHT_PURPLE),
    KAPELLE(Material.MUSIC_DISC_WAIT, ChatColor.AQUA),
    FRIEDHOF(Material.MUSIC_DISC_CHIRP, ChatColor.DARK_RED),
    HEXENBAUM(Material.MUSIC_DISC_STAL, ChatColor.DARK_PURPLE),
    STEINKREIS(Material.MUSIC_DISC_13, ChatColor.GRAY),
    PLAYER_BLUE(Material.BLUE_WOOL, ChatColor.BLUE),
    PLAYER_GRAY(Material.GRAY_WOOL, ChatColor.GRAY),
    PLAYER_YELLOW(Material.YELLOW_WOOL, ChatColor.YELLOW),
    PLAYER_BLACK(Material.BLACK_WOOL, ChatColor.DARK_GRAY),
    PLAYER_GREEN(Material.LIME_WOOL, ChatColor.GREEN),
    PLAYER_PURPLE(Material.PURPLE_WOOL, ChatColor.DARK_PURPLE),
    PLAYER_ORANGE(Material.ORANGE_WOOL, ChatColor.GOLD),
    PLAYER_RED(Material.RED_WOOL, ChatColor.RED),
    ITEM_ABILITY(Material.ENCHANTED_BOOK, ChatColor.GREEN),
    END_ROUND(Material.LIME_WOOL, ChatColor.GREEN),
    ITEM_QUIT(Material.SPRUCE_DOOR, ChatColor.DARK_BLUE),
    ITEM_SHOW_IDENTITY(Material.PLAYER_HEAD, ChatColor.GOLD);


    private final Material material;
    private final ChatColor color;

    ClickableItems(Material material, ChatColor color) {
        this.material = material;
        this.color = color;
    }


    public Material getMaterial() {
        return material;
    }

    public String getColor() {
        return color.toString();
    }

    public ClickableItems getClickableItems(String title, String type, String color) {
        ClickableItems c = null;
        if(type.equalsIgnoreCase("Ausrüstung")) {
            switch (title) {
                case "Armbrust" -> c = ClickableItems.ARMBRUST;
                case "Fackel" -> c = ClickableItems.FACKEL;
                case "Eisenfaust" -> c = ClickableItems.EISENFAUST;
                case "Feuerzauber" -> c = ClickableItems.FEUERZAUBER;
                case "Magischer Kompass" -> c = ClickableItems.MAGISCHER_KOMPASS;
                case "Knochenlanze" -> c = ClickableItems.KNOCHENLANZE;
                case "Talisman" -> c = ClickableItems.TALISMAN;
                case "Schutzamulett" -> c = ClickableItems.SCHUTZAMULETT;
                case "Schutzring" -> c = ClickableItems.SCHUTZRING;
                case "Schutzumhang" -> c = ClickableItems.SCHUTZUMHANG;
                case "Rucksack" -> c = ClickableItems.RUCKSACK;
            }
        } else if(type.equalsIgnoreCase("Ereignis")){
            if(color.equalsIgnoreCase("red")) {
                c = ClickableItems.RED_CARD;
            } else if(color.equalsIgnoreCase("blue")) {
                c = ClickableItems.BLUE_CARD;
            }
        } else if(type.equalsIgnoreCase("Oxana spricht!")) {
            c = ClickableItems.GREEN_CARD;
        }
        return c;
    }

    public ItemStack getItem(String title, String loreText) {
        ItemStack item = new ItemStack(material);
        ItemMeta meta = item.getItemMeta();
        assert meta != null;
        ArrayList<String> lore = new ArrayList<>();
        String[] splittedLoreText = loreText.split("\\s");
        for (int i = 0; i < splittedLoreText.length; i+=4) {
            String s = ChatColor.GRAY + splittedLoreText[i];
            if(splittedLoreText.length > i+1)
                s += " " + ChatColor.GRAY + splittedLoreText[i+1];
            if(splittedLoreText.length > i+2)
                s += " " + ChatColor.GRAY + splittedLoreText[i+2];
            if(splittedLoreText.length > i+3)
                s += " " + ChatColor.GRAY + splittedLoreText[i+3];
            lore.add(s);
        }
        meta.setLore(lore);
        meta.setDisplayName(color+title);
        item.setItemMeta(meta);
        return item;
    }
}
