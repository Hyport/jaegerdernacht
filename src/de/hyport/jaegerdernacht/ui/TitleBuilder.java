package de.hyport.jaegerdernacht.ui;

import de.hyport.jaegerdernacht.main.GameController;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;

public class TitleBuilder {

    private final Plugin plugin;
    private final Player player;
    public TitleBuilder(Plugin plugin, Player player) {
        this.plugin = plugin;
        this.player = player;
    }

    public long rollDiceAnimation(int number, int maxValue, String subtitle, ChatColor mainColor, int baseDelay) {
        int count = (int) (Math.random()*(10))+maxValue+1;
        long lastDelay = baseDelay;
        for (int i = 0; i < count; i++) {
            int currentNumber = (i+maxValue+(number-count%maxValue))
                    %maxValue+1;
            if (i!= count-1) {
                lastDelay = sendDelayedTitle(lastDelay, i+2, mainColor.toString()+currentNumber, subtitle, Sound.BLOCK_DISPENSER_FAIL);
            } else{
                lastDelay = sendDelayedTitle(lastDelay, 65, mainColor.toString()+currentNumber, subtitle, Sound.BLOCK_NOTE_BLOCK_PLING);
                Bukkit.getScheduler().runTaskLater(plugin, () -> {
                    GameController.diceResult.set(number);
                    GameController.diceDone.set(true);
                },lastDelay);
            }
        }
        return lastDelay;
    }

    public long sendDelayedTitle(long delay, int duration, String title, String subtitle, Sound sound) {
        new BukkitRunnable() {
            @Override
            public void run() {
                for (Player p : Bukkit.getOnlinePlayers()) {
                    String st = "";
                    if (p != player) {
                        st = player.getDisplayName();
                    }
                    p.playSound(p.getLocation(), sound, 1, 1);
                    p.sendTitle(title, st, 0, duration, 0);
                }
            }
        }.runTaskLater(plugin, delay);
        return delay+duration;
    }

}
