package de.hyport.jaegerdernacht.ui;

import de.hyport.jaegerdernacht.main.*;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicBoolean;

public class CardAnimation {

    private Inventory inv;
    private final Plugin plugin;
    private final ArrayList<Card> cards = new ArrayList<>();
    private final ArrayList<Card> cardsInInv = new ArrayList<>();
    static final AtomicBoolean endScreenOpen = new AtomicBoolean(false);
    public CardAnimation(Plugin plugin) {
        this.plugin = plugin;
    }

    private int count;
    private int delay;
    private boolean nextStep = false;
    private CardStack finalCardstack;

    public Card playAnimation(GamePlayer gamePlayer, CardStack cardStack) {
        String stackColor = cardStack.getColor();
        delay = 1;
        count = 17;
        Player p = gamePlayer.getPlayer();
        finalCardstack = cardStack;

        AtomicBoolean running = new AtomicBoolean(false);
        inv = null;
        inv = Bukkit.createInventory(null, 27, InventoryListener.ZIEHE_KARTE);

        switch (stackColor) {
            case("green"):
                for(int i = 0; i < 27; i++) {
                    inv.setItem(i, new ItemBuilder(Material.GREEN_STAINED_GLASS_PANE).setName(" ").build());
                }
                inv.setItem(4, new ItemBuilder(Material.SPRUCE_SIGN).setName("§2Grüne Karte:").setLore(
                        "§7-------------------------",
                        "§7Du ziehst eine §2Grüne §7Karte.",
                        "§7Die Karte die am Ende hier",
                        "§7stehen bleibt, ist §edeine§7!",
                        "§7-------------------------").build());
                inv.setItem(22, new ItemBuilder(Material.SPRUCE_SIGN).setName("§2Grüne Karte:").setLore(
                        "§7-------------------------",
                        "§7Du ziehst eine §2Grüne §7Karte.",
                        "§7Die Karte die am Ende hier",
                        "§7stehen bleibt, ist §edeine§7!",
                        "§7-------------------------").build());
                break;
            case("red"):
                for(int i = 0; i < 27; i++) {
                    inv.setItem(i, new ItemBuilder(Material.RED_STAINED_GLASS_PANE).setName(" ").build());
                }
                inv.setItem(4, new ItemBuilder(Material.SPRUCE_SIGN).setName("§cRote Karte:").setLore(
                        "§7-------------------------",
                        "§7Du ziehst eine §cRote §7Karte, die",
                        "§7entweder eine §eAusrüstung" ,
                        "§7oder §eEreignis §7Karte ist.",
                        "§7Die Karte die am Ende hier",
                        "§7stehen bleibt, ist §edeine§7!",
                        "§7-------------------------").build());
                inv.setItem(22, new ItemBuilder(Material.SPRUCE_SIGN).setName("§cRote Karte:").setLore(
                        "§7-------------------------",
                        "§7Du ziehst eine §cRote §7Karte, die",
                        "§7entweder eine §eAusrüstung" ,
                        "§7oder §eEreignis §7Karte ist.",
                        "§7Die Karte die am Ende hier",
                        "§7stehen bleibt, ist §edeine§7!",
                        "§7-------------------------").build());

                break;
            case("blue"):
                for(int i = 0; i < 27; i++) {
                    inv.setItem(i, new ItemBuilder(Material.BLUE_STAINED_GLASS_PANE).setName(" ").build());
                }
                inv.setItem(4, new ItemBuilder(Material.SPRUCE_SIGN).setName("§9Blaue Karte:").setLore(
                        "§7-------------------------",
                        "§7Du ziehst eine §9Blaue §7Karte, die",
                        "§7entweder eine §eAusrüstung" ,
                        "§7oder §eEreignis §7Karte ist.",
                        "§7Die Karte die am Ende hier",
                        "§7stehen bleibt, ist §edeine§7!",
                        "§7-------------------------").build());
                inv.setItem(22  , new ItemBuilder(Material.SPRUCE_SIGN).setName("§9Blaue Karte:").setLore(
                        "§7-------------------------",
                        "§7Du ziehst eine §9Blaue §7Karte, die",
                        "§7entweder eine §eAusrüstung" ,
                        "§7oder §eEreignis §7Karte ist.",
                        "§7Die Karte die am Ende hier",
                        "§7stehen bleibt, ist §edeine§7!",
                        "§7-------------------------").build());

                break;
        }

        cards.clear();
        cardsInInv.clear();

        fillCard(cardStack);

        Bukkit.getScheduler().runTask(plugin, ()-> {
            p.openInventory(inv);
        });

        fillInventory();


        running.set(true);
        nextStep = true;

        while (running.get()) {
            if(nextStep) {
                p.playSound(p.getLocation(), Sound.BLOCK_NOTE_BLOCK_BIT, 1, 1);
                nextStep = false;
                nextTask(delay);
                this.delay = delay + 1;
                count--;
                refillItems();
            }

            if(count == 0) {
                p.playSound(p.getLocation(), Sound.BLOCK_NOTE_BLOCK_FLUTE, 1, 1);
                running.set(false);
            }
        }

        Card c = cardsInInv.get(4);
        cardStack.remove(c);
        GameController.drawCard = c;
        endScreenOpen.set(true);
        endScreen();
        while(endScreenOpen.getAndSet(false)){
            try {
                Thread.sleep(100);
            } catch (Exception ignored){}
        }
        return c;
    }

    private void endScreen() {
        for(int i = 0;i<26;i++) {
            if(i!=13) {
                inv.setItem(i, new ItemBuilder(Material.GRAY_STAINED_GLASS_PANE).setName(" ").build());
            }
        }
        inv.setItem(26, new ItemBuilder(Material.ARROW).setName("§2Weiter:").setLore(
                "§7-----------------------------------",
                "§eRechtsklick §7um das Fenster zu schließen!",
                "§7-----------------------------------"

        ).build());
    }

    private void nextTask(int delay) {

        BukkitRunnable runnable = new BukkitRunnable() {
            @Override
            public void run() {
                nextStep = true;

            }
        };runnable.runTaskLater(plugin, delay);
    }

    /*  0  1  2  3  4  5  6  7  8
     *  9 10 11 12 13 14 15 16 17
     * 18 19 20 21 22 23 24 25 26
     */

    public void refillItems() {

       for(int i = 17;i>=10;i--) {
           inv.setItem(i, inv.getItem(i-1));
       }
       cards.add(cardsInInv.get(8));
       for(int i = 8; i>0;i--) {
           cardsInInv.set(i, cardsInInv.get(i-1));
       }
       inv.setItem(9, getItembuilder(cards.get(0)).build());
       cardsInInv.set(0, cards.get(0));
       cards.remove(0);
    }

    public void fillInventory() {
        ArrayList<Card> addCards = new ArrayList<>();
        while(cards.size() < 10) {
            for(Card c : cards) {
                Card card = new Card(c.getType(), c.getTitle(), c.getText(), c.getColor());
                addCards.add(card);
            }
            cards.addAll(addCards);
            addCards.clear();
        }

        for(int i = 0; i<= 8;i++) {
            inv.setItem(i + 9, getItembuilder(cards.get(0)).build());
            cardsInInv.add(cards.get(0));
            cards.remove(0);
        }
    }

    public ItemBuilder getItembuilder(Card card) {
        ClickableItems c = getClickableItems(card.getTitle(), card.getType(), card.getColor());
        return new ItemBuilder(c.getMaterial()).setName(card.getChatColor() + card.getTitle()).setLore(createLore(card.getText()));
    }

    public void fillCard(CardStack cardStack) {
        for(int i = 0; i < cardStack.size();i++) {
            cards.add(cardStack.get(i));
        }
    }

    private ArrayList<String> createLore(String text) {
        ArrayList<String> lore = new ArrayList<>();
        String[] splittedLoreText = text.split("\\s");
        for (int i = 0; i < splittedLoreText.length; i+=4) {
            String s = ChatColor.GRAY + splittedLoreText[i];
            if(splittedLoreText.length > i+1)
                s += " " + ChatColor.GRAY + splittedLoreText[i+1];
            if(splittedLoreText.length > i+2)
                s += " " + ChatColor.GRAY + splittedLoreText[i+2];
            if(splittedLoreText.length > i+3)
                s += " " + ChatColor.GRAY + splittedLoreText[i+3];
            lore.add(s);
        }
        return lore;
    }


    public ClickableItems getClickableItems(String title, String type, String color) {
        ClickableItems c = null;
        if(type.equalsIgnoreCase("Ausrüstung")) {
            switch (title) {
                case "Armbrust" -> c = ClickableItems.ARMBRUST;
                case "Fackel" -> c = ClickableItems.FACKEL;
                case "Eisenfaust" -> c = ClickableItems.EISENFAUST;
                case "Feuerzauber" -> c = ClickableItems.FEUERZAUBER;
                case "Magischer Kompass" -> c = ClickableItems.MAGISCHER_KOMPASS;
                case "Knochenlanze" -> c = ClickableItems.KNOCHENLANZE;
                case "Talisman" -> c = ClickableItems.TALISMAN;
                case "Schutzamulett" -> c = ClickableItems.SCHUTZAMULETT;
                case "Schutzring" -> c = ClickableItems.SCHUTZRING;
                case "Schutzumhang" -> c = ClickableItems.SCHUTZUMHANG;
                case "Rucksack" -> c = ClickableItems.RUCKSACK;
            }
        } else if(type.equalsIgnoreCase("Ereignis")){
            if(color.equalsIgnoreCase("red")) {
                c = ClickableItems.RED_CARD;
            } else if(color.equalsIgnoreCase("blue")) {
                c = ClickableItems.BLUE_CARD;
            }
        } else if(type.equalsIgnoreCase("Oxana spricht!")) {
            c = ClickableItems.GREEN_CARD;
        }
        return c;
    }

}
