package de.hyport.jaegerdernacht.ui;

import de.hyport.jaegerdernacht.main.Card;
import de.hyport.jaegerdernacht.main.Field;
import de.hyport.jaegerdernacht.main.GameController;
import de.hyport.jaegerdernacht.main.GamePlayer;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.plugin.Plugin;

import java.util.ArrayList;
import java.util.LinkedList;

public class InventoryBuilder {

    /*  0  1  2  3  4  5  6  7  8
     *  9 10 11 12 13 14 15 16 17
     * 18 19 20 21 22 23 24 25 26
     */

    private final Inventory inventory;
    private final Player player;
    public InventoryBuilder(Player p, String title) {
        this.inventory = Bukkit.createInventory(p, 27, title);
        this.player = p;
    }

    public InventoryBuilder createBoolInventory(String title, String question) {
        inventory.setItem(4, ClickableItems.ITEM_QUESTION.getItem(title, question));
        inventory.setItem(11, ClickableItems.ITEM_YES.getItem("JA", ""));
        inventory.setItem(15, ClickableItems.ITEM_NO.getItem("NEIN", ""));
        return this;
    }

    public InventoryBuilder createPlayerInventar(GameController gameController, GamePlayer inventoryOwner) {
        for(int i = 0; i < 27; i++) {
            inventory.setItem(i, new ItemBuilder(Material.GRAY_STAINED_GLASS_PANE).setName(" ").build());
        }
        inventory.setItem(4, new ItemBuilder(Material.PLAYER_HEAD).setName("§aSpielerübersicht:").setLore(
                "§7-------------------------------",
                "§7Hier kannst du alle §aSpieler",
                "§7mit deren §eAusrüstungskarten",
                "§7sehen und Vermutungen zu deren",
                "§bRollen §7abspeichern",
                "§7-------------------------------").build());

        LinkedList<GamePlayer> gamePlayers = new LinkedList<>(gameController.getPlayers());
        gamePlayers.remove(inventoryOwner);
        String roleName = null;
        ChatColor color = null;
        String guessedRole = null;

        for(int i = 0; i < gamePlayers.size();i++) {
            ArrayList<String> lore = new ArrayList<>();
            roleName = inventoryOwner.getGuesses().get(gamePlayers.get(i));
            player.sendMessage();
            if (gamePlayers.get(i).getRole().isRevealed()) {
                switch (gamePlayers.get(i).getRole().getRoleName()) {
                    case "Werwolf" -> color = ChatColor.BLUE;
                    case "Vampir" -> color = ChatColor.RED;
                    case "Mensch" -> color = ChatColor.YELLOW;
                }
                lore.add("§7-------------------------------------");
                lore.add("§7Der Spieler §e" + gamePlayers.get(i).getPlayer().getDisplayName() + "§7 hat seine");
                lore.add("§7Charakterkarte aufgedeckt und ist " + color + gamePlayers.get(i).getRole().getName());
                lore.add("§7-------------------------------------");
                lore.add("§eAusrüstungskarten:");
                if(gamePlayers.get(i).getInventory().size() == 0) {
                    lore.add("§c keine");
                }
                for (int j = 0; j < gamePlayers.get(i).getInventory().size(); j++) {
                    lore.add("§7- " + gamePlayers.get(i).getInventory().get(j).getChatColor() + gamePlayers.get(i).getInventory().get(j).getTitle());
                }
                lore.add("§7-------------------------------------");
                lore.add("§eRechtsklick §7um eine Vermutung abzuspeichern");
                lore.add("§eLinksklick §7um die §eAusrüstungskarten §7anzuschauen");
                if (!gamePlayers.get(i).equals(inventoryOwner))
                    inventory.setItem(i + 9, new SkullBuilder(gamePlayers.get(i).getPlayer()).setLore(lore).build());
            } else if (inventoryOwner.getGuesses().get(gamePlayers.get(i)).equalsIgnoreCase("leer")) {
                color = ChatColor.WHITE;
                lore.add("§7-------------------------------------");
                lore.add("§7Für dem Spieler §b" + gamePlayers.get(i).getPlayer().getDisplayName() + "§7 hast du");
                lore.add("§7noch keine Vermutung abgegeben");
                lore.add("§7-------------------------------------");
                lore.add("§eAusrüstungskarten:");
                if(gamePlayers.get(i).getInventory().size() == 0) {
                    lore.add("§c keine");
                }
                for (int j = 0; j < gamePlayers.get(i).getInventory().size(); j++) {
                    lore.add("§7- " + gamePlayers.get(i).getInventory().get(j).getChatColor() + gamePlayers.get(i).getInventory().get(j).getTitle());
                }
                lore.add("§7-------------------------------------");
                lore.add("§eRechtsklick §7um eine Vermutung abzuspeichern");
                lore.add("§eLinksklick §7um die §eAusrüstungskarten §7anzuschauen");
                if (!gamePlayers.get(i).equals(inventoryOwner))
                    inventory.setItem(i + 9, new SkullBuilder(gamePlayers.get(i).getPlayer()).setLore(lore).build());
                lore.clear();
            } else {
                switch (roleName) {
                    case "Werwolf" -> color = ChatColor.BLUE;
                    case "Vampir" -> color = ChatColor.RED;
                    case "Mensch" -> color = ChatColor.YELLOW;
                }
                guessedRole = inventoryOwner.getGuesses().get(gamePlayers.get(i));
                lore.add("§7-------------------------------------");
                lore.add("§§7Du vermutest, dass der Spieler §e");
                lore.add(gamePlayers.get(i).getPlayer().getDisplayName() + "§7 ein " + color + guessedRole + "§7 ist.");
                lore.add("§7-------------------------------------");
                lore.add("§eAusrüstungskarten:");
                if(gamePlayers.get(i).getInventory().size() == 0) {
                    lore.add("§c keine");
                }
                for (int j = 0; j < gamePlayers.get(i).getInventory().size(); j++) {
                    lore.add("§7- " + gamePlayers.get(i).getInventory().get(j).getChatColor() + gamePlayers.get(i).getInventory().get(j).getTitle());
                }
                lore.add("§7-------------------------------------");
                lore.add("§eRechtsklick §7um eine Vermutung abzuspeichern");
                lore.add("§eLinksklick §7um die §eAusrüstungskarten §7anzuschauen");
                if (!gamePlayers.get(i).equals(inventoryOwner))
                    inventory.setItem(i + 9, new SkullBuilder(gamePlayers.get(i).getPlayer()).setLore(lore).build());
            }

        }
        return this;
    }

    public InventoryBuilder createChooseGuessInventory(GamePlayer inventoryOwner, GamePlayer chooseRole) {
        for(int i = 0; i < 27; i++) {
            inventory.setItem(i, new ItemBuilder(Material.GRAY_STAINED_GLASS_PANE).setName(" ").build());
        }
        inventory.setItem(4, new ItemBuilder(Material.SPRUCE_SIGN).setName("§e" + chooseRole.getPlayer().getDisplayName()).setLore(
                "§7-------------------------------",
                "§7Hier kannst du eine Vermutung",
                "§7zu der Rolle von §e" + chooseRole.getPlayer().getDisplayName(),
                "§7abspeichern",
                "§7-------------------------------").build());
        inventory.setItem(11, new ItemBuilder(Material.RED_DYE).setName("§cVampir").setLore(
                "§7-------------------------------",
                "§eRechtsklick§7, um die Vermutung",
                "§7abzuspeichern, dass " + chooseRole.getPlayer().getDisplayName(),
                "§7 ein §cVampire §7ist!",
                "§7-------------------------------").build());

        inventory.setItem(13, new ItemBuilder(Material.BLUE_DYE).setName("§9Werwolf").setLore(
                "§7-------------------------------",
                "§eRechtsklick§7, um die Vermutung",
                "§7abzuspeichern, dass " + chooseRole.getPlayer().getDisplayName(),
                "§7 ein §9Werwolf §7ist!",
                "§7-------------------------------").build());
        inventory.setItem(15, new ItemBuilder(Material.YELLOW_DYE).setName("§eMensch").setLore(
                "§7-------------------------------",
                "§eRechtsklick§7, um die Vermutung",
                "§7abzuspeichern, dass " + chooseRole.getPlayer().getDisplayName(),
                "§7 ein §eMensch §7ist!",
                "§7-------------------------------").build());
        return this;
    }

    public InventoryBuilder seeAllCards(GamePlayer gamePlayer) {
        for(int i = 0; i < 27; i++) {
            inventory.setItem(i, new ItemBuilder(Material.GRAY_STAINED_GLASS_PANE).setName(" ").build());
        }
        inventory.setItem(4, new ItemBuilder(Material.SPRUCE_SIGN).setName("§eAusrüstungskarten §7von §e " + gamePlayer.getPlayer().getDisplayName()).setLore(
                "§7-------------------------------",
                "§7Hier siehst du alle Ausrüstungskarten",
                "§7von §e" + gamePlayer.getPlayer().getDisplayName(),
                "§7-------------------------------").build());
        for (int i = 0, itemIndex = 10; i < gamePlayer.getInventory().size(); i++, itemIndex++) {
            Card card = gamePlayer.getInventory().get(i);
            inventory.setItem(itemIndex, getCorrectItem(card).getItem(card.getTitle(), card.getText()));
        }

        return this;
    }

    public InventoryBuilder createQuelleDerWeisheitInventar() {
        for(int i = 0; i < 27; i++) {
            inventory.setItem(i, new ItemBuilder(Material.GRAY_STAINED_GLASS_PANE).setName(" ").build());
        }
        inventory.setItem(11, new ItemBuilder(Material.GREEN_WOOL).setName("§2Grüner Stapel").setLore(
                "§7--------------------------",
                "§eRechtsklick§7, um eine",
                "§2grüne §7Karte zu ziehen",
                "§7--------------------------"
        ).build());
        inventory.setItem(13, new ItemBuilder(Material.BLUE_WOOL).setName("§9Blauer Stapel").setLore(
                "§7--------------------------",
                "§eRechtsklick§7, um eine",
                "§9blaue §7Karte zu ziehen",
                "§7--------------------------"
        ).build());
        inventory.setItem(15, new ItemBuilder(Material.RED_WOOL).setName("§cRoter Stapel").setLore(
                "§7--------------------------",
                "§eRechtsklick§7, um eine",
                "§crote §7Karte zu ziehen",
                "§7--------------------------"
        ).build());
        return this;
    }

    public InventoryBuilder createCardInventory(ArrayList<Card> cards) {
        Card card;
        for (int i = 0, itemIndex = 10; i < cards.size(); i++, itemIndex++) {
            card = cards.get(i);
            inventory.setItem(itemIndex, getCorrectItem(card).getItem(card.getTitle(), card.getText()));
        }

        return this;
    }

    
    public InventoryBuilder createFieldInventory(LinkedList<Field> allFields, LinkedList<GamePlayer> allPlayers) {
        int itemIndex = 10;
        for (Field f : allFields) {
            if(getCorrectItem(f) != null) {
                if (itemIndex == 13) itemIndex++;

                StringBuilder loreText = new StringBuilder();
                for (GamePlayer gp : f.getPlayers(allPlayers)) {
                    loreText.append(gp.getPlayer().getDisplayName()).append("§r\n");
                }
                inventory.setItem(itemIndex, getCorrectItem(f).getItem(f.getColor() + f.getName(), loreText.toString()));
            }
            itemIndex++;
        }
        return this;
    }


    public InventoryBuilder createPlayerInventory(LinkedList<GamePlayer> players, String title, String question) {
        inventory.setItem(4, ClickableItems.ITEM_QUESTION.getItem(title, question));
        GamePlayer gp;
        for (int i = 0; i < players.size(); i++) {
            gp = players.get(i);
            if (gp.isAlive()) {
                ItemStack skull = new ItemStack(Material.PLAYER_HEAD);
                SkullMeta meta = (SkullMeta) skull.getItemMeta();
                assert meta != null;
                meta.setOwningPlayer(Bukkit.getOfflinePlayer(gp.getPlayer().getUniqueId()));
                meta.setDisplayName(gp.getPlayer().getDisplayName());
                skull.setItemMeta(meta);
                inventory.setItem(i + 9, skull);
            }
        }
        return this;
    }

    public InventoryBuilder createGreenCardInventory(String title, String question) {
        inventory.setItem(13, ClickableItems.GREEN_CARD.getItem(title, question));
        inventory.setItem(26, ClickableItems.END_ROUND.getItem("Karte geben", ""));
        return this;
    }

    public InventoryBuilder createGreenCardConsequenceChooseInventory(GamePlayer target, String title, String question) {
        inventory.setItem(4, ClickableItems.GREEN_CARD.getItem(title, question));
        inventory.setItem(11, ClickableItems.ITEM_NO.getItem("Schaden", ""));
        inventory.setItem(15, ClickableItems.ITEM_YES.getItem("Heilung", ""));
        inventory.setItem(21, ClickableItems.ITEM_SHOW_IDENTITY.getItem("Zeige Identität", ""));
        inventory.setItem(23, ClickableItems.ITEM_QUIT.getItem("Nichts geschieht", ""));
        if (target.getInventory().containsEquipmentCard()) {
            inventory.setItem(13, ClickableItems.ITEM_DRAW_CARD.getItem("Karte geben", ""));
        }
        return this;
    }

    public InventoryBuilder createColorInventory(LinkedList<GamePlayer> allPlayers) {
        setColorItem(9, allPlayers, ClickableItems.PLAYER_BLACK, "SCHWARZ");
        setColorItem(10, allPlayers, ClickableItems.PLAYER_GRAY, "GRAU");
        setColorItem(11, allPlayers, ClickableItems.PLAYER_YELLOW, "GELB");
        setColorItem(12, allPlayers, ClickableItems.PLAYER_BLUE, "BLAU");
        setColorItem(14, allPlayers, ClickableItems.PLAYER_RED, "ROT");
        setColorItem(15, allPlayers, ClickableItems.PLAYER_GREEN, "GRÜN");
        setColorItem(16, allPlayers, ClickableItems.PLAYER_ORANGE, "ORANGE");
        setColorItem(17, allPlayers, ClickableItems.PLAYER_PURPLE, "LILA");
        return this;
    }

    private void setColorItem(int index, LinkedList<GamePlayer> allPlayers, ClickableItems item, String colorName) {
        String color = item.getColor();
        if (!colorReserved(color, allPlayers)) {
            inventory.setItem(index, createItem(item.getMaterial(), color+colorName));
        }
    }

    private boolean colorReserved(String color, LinkedList<GamePlayer> allPlayers) {
        for (GamePlayer gp : allPlayers) {
            if (gp.getPlayerColor().equals(color)) {
                return true;
            }
        }
        return false;
    }

    public static <T> ClickableItems getCorrectItem(T  t) {
        ClickableItems c = null;
        if (t instanceof Card card) {
            switch (card.getTitle()) {
                case "Armbrust" -> c = ClickableItems.ARMBRUST;
                case "Fackel" -> c = ClickableItems.FACKEL;
                case "Eisenfaust" -> c = ClickableItems.EISENFAUST;
                case "Feuerzauber" -> c = ClickableItems.FEUERZAUBER;
                case "Magischer Kompass" -> c = ClickableItems.MAGISCHER_KOMPASS;
                case "Knochenlanze" -> c = ClickableItems.KNOCHENLANZE;
                case "Talisman" -> c = ClickableItems.TALISMAN;
                case "Schutzamulett" -> c = ClickableItems.SCHUTZAMULETT;
                case "Schutzring" -> c = ClickableItems.SCHUTZRING;
                case "Schutzumhang" -> c = ClickableItems.SCHUTZUMHANG;
                case "Rucksack" -> c = ClickableItems.RUCKSACK;
                default -> card.getChatColor();
            }
        } else if (t instanceof Field f){
            switch (f.getName()) {
                case "Oxanas Hütte" -> c = ClickableItems.OXANAS_HUETTE;
                case "Quell der Weisheit" -> c = ClickableItems.QUELL_DER_WEISHEIT;
                case "Kapelle" -> c = ClickableItems.KAPELLE;
                case "Friedhof" -> c = ClickableItems.FRIEDHOF;
                case "Hexenbaum" -> c = ClickableItems.HEXENBAUM;
                case "Steinkreis" -> c = ClickableItems.STEINKREIS;
            }
        }

        return c;
    }

    public static ItemStack createItem(Material material, String displayName) {
        ItemStack item = new ItemStack(material);
        ItemMeta meta = item.getItemMeta();
        assert meta != null;
        meta.setDisplayName(displayName);
        item.setItemMeta(meta);
        return item;
    }

    public void build(Plugin plugin) {
        Bukkit.getScheduler().runTask(plugin,() -> player.openInventory(inventory));
    }

}
