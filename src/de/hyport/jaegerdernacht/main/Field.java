package de.hyport.jaegerdernacht.main;

import de.hyport.jaegerdernacht.ui.InventoryBuilder;
import de.hyport.jaegerdernacht.ui.InventoryListener;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;

import java.util.LinkedList;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

public enum Field {
    OXANA("Oxanas Hütte", ChatColor.DARK_GREEN, 15, 84, 33, -61, 1, 0),
    QUELL("Quell der Weisheit", ChatColor.LIGHT_PURPLE, 30, 84, 17, -116, 0, 0),
    KAPELLE("Kapelle", ChatColor.AQUA, 23, 85, -7, -158, 0, 1),
    FRIEDHOF("Friedhof", ChatColor.DARK_RED, 4, 85, -14, 127, 6, 1),
    HEXENBAUM("Hexenbaum", ChatColor.DARK_PURPLE, -8, 86, 3, 87, -2, 2),
    STEINKREIS("Steinkreis", ChatColor.GRAY, -10, 85, 30, 30, 3, 2),
    MITTE("Mitte", ChatColor.WHITE, 7, 86, 1, 0, -22, -1);

    public static final AtomicReference<GamePlayer> GREEN_CARD_SOURCE = new AtomicReference<>(); // player who distributes green card

    private final String name;
    private final ChatColor color;
    private final int x;
    private final int y;
    private final int z;
    private final float yaw;
    private final float pitch;
    private final int area; // Gebiet
    Field(String name, ChatColor color, int x, int y, int z, float yaw, float pitch, int area) {
        this.name = name;
        this.color = color;
        this.x = x;
        this.y = y;
        this.z = z;
        this.yaw = yaw;
        this.pitch = pitch;
        this.area = area;
    }


    /**
     *
     * @param allPlayers list of GamePlayers which will be checked
     * @return players who stand on this field
     */
    public LinkedList<GamePlayer> getPlayers(LinkedList<GamePlayer> allPlayers) {
        LinkedList<GamePlayer> players = new LinkedList<>();
        for (GamePlayer gp : allPlayers) {
            if (gp.getField() == this) {
                players.add(gp);
            }
        }
        return players;
    }

    public String getName() {
        return name;
    }

    public ChatColor getColor() {
        return color;
    }

    public int getArea() {
        return area;
    }

    public Field getNeighbourField() {
        for(Field f : Field.values()) {
            if(f.area == area && f != this) {
                return f;
            }
        }
        return MITTE;
    }

    public Location getLocation() {
        return new Location(Bukkit.getWorld("world"), x, y, z, yaw, pitch);
    }

    public void startAction(GamePlayer gamePlayer) {
        GamePlayer target;
        switch (this) {
            case OXANA -> drawGreenCard(gamePlayer, true);
            case QUELL -> {
                // choose card stack
                InventoryListener.quellChoiceMade.set(false);
                new InventoryBuilder(gamePlayer.getPlayer(), "Quell der Weisheit:").createQuelleDerWeisheitInventar()
                        .build(gamePlayer.getGameController().getPlugin());
                String response = gamePlayer.getGameController().waitForUserInput(gamePlayer.getPlayer());
                // execute card based on choice
                if (response.contains("Blauer Stapel")) {
                    drawBlueOrRedCard(gamePlayer, gamePlayer.getGameController().getBlueStack());
                } else if (response.contains("Roter Stapel")) {
                    drawBlueOrRedCard(gamePlayer, gamePlayer.getGameController().getRedStack());
                } else if (response.contains("Grüner Stapel")) {
                    drawGreenCard(gamePlayer, true);
                }
            }
            case HEXENBAUM -> {
                target = gamePlayer.getGameController().chooseTargetFromList(gamePlayer.getPlayer(), gamePlayer.getGameController().getPlayers(), getName(), "Wähle einen Spieler.");
                if (target == gamePlayer) {
                    gamePlayer.decreaseDamage(1, DamageCause.HEXENBAUM);
                } else if (target != null) {
                    target.increaseDamage(2, DamageCause.HEXENBAUM, gamePlayer);
                }
            }
            case KAPELLE -> drawBlueOrRedCard(gamePlayer, gamePlayer.getGameController().getBlueStack());
            case FRIEDHOF -> drawBlueOrRedCard(gamePlayer, gamePlayer.getGameController().getRedStack());
            case STEINKREIS -> {
                LinkedList<GamePlayer> targetList = gamePlayer.getPlayersWithEquipmentCards();
                if (targetList.size() > 0) {
                    target = gamePlayer.getGameController().chooseTargetFromList(gamePlayer.getPlayer(), targetList, getName(), "Von wem möchtest du eine Karte stehlen?");
                    if (target != null) {
                        Card chosenCard = gamePlayer.getGameController().chooseCardFromInventory(gamePlayer.getPlayer(), target.getInventory());
                        if (chosenCard != null) {
                            Bukkit.broadcastMessage(target.getPlayer().getDisplayName() + " gibt " + chosenCard.getChatColor() + chosenCard.getTitle() + ChatColor.RESET + " an " + gamePlayer.getPlayer().getDisplayName() + "!");
                            target.getInventory().remove(chosenCard);
                            gamePlayer.getInventory().add(chosenCard);
                        }
                    }
                }
            }
        }
    }

    /**
     * Draws a card from the blue or red CardStack
     * @param gamePlayer the GamePlayer which draws the card
     * @param cardStack the desired CardStack
     */
    public void drawBlueOrRedCard(GamePlayer gamePlayer, CardStack cardStack) {
        Card card = cardStack.drawCard(gamePlayer);
        Bukkit.broadcastMessage(gamePlayer.getPlayer().getDisplayName() + " hat " + card.getChatColor() + card.getTitle() +ChatColor.RESET + " gezogen!");
        if (card.getType().equalsIgnoreCase("Ereignis")) {
            card.executeCard(gamePlayer);
        } else {
            gamePlayer.getInventory().add(card);
        }
    }

    /**
     * Draws a card from the green CardStack
     * @param gamePlayer the GamePlayer which draws the card
     */
    public void drawGreenCard(GamePlayer gamePlayer, boolean normal) {
        GameController controller = gamePlayer.getGameController();
        Card card = controller.getGreenStack().pop();
        new InventoryBuilder(gamePlayer.getPlayer(), "Lies dir die Karte durch!").createGreenCardInventory(card.getTitle(), card.getText()).build(controller.getPlugin());
        controller.waitForUserInput(gamePlayer.getPlayer());
        GamePlayer target;
        if (normal) {
            target = controller.chooseTarget(gamePlayer, "Wem möchtest du diese Karte geben?", card.getText());
        } else if (controller.getCurrentPlayerIndex()+1 < controller.getPlayers().size()) {
            target = controller.getPlayers().get(controller.getCurrentPlayerIndex()+1);
        } else {
            target = controller.getPlayers().get(0);
        }
        if (target != null) {
            GREEN_CARD_SOURCE.set(gamePlayer);
            Bukkit.broadcastMessage(gamePlayer.getPlayer().getDisplayName() + " gibt eine " + ChatColor.DARK_GREEN + "grüne" + ChatColor.RESET + " Karte an " + target.getPlayer().getDisplayName() + "!");
            new InventoryBuilder(target.getPlayer(), InventoryListener.GRUENE_KARTE).createGreenCardConsequenceChooseInventory(target, card.getTitle(), card.getText()).build(controller.getPlugin());
            controller.waitForUserInput(target.getPlayer());
            gamePlayer.getGameController().getGreenDiscard().push(card);
        }
    }

    /**
     *
     * @param number the diced number (2-10)
     * @return The Field which matches the number
     */
    public static Field getField(int number) {
        Field field;
        switch (number) {
            case 2, 3 -> field = OXANA;
            case 4, 5 -> field = QUELL;
            case 6 -> field = KAPELLE;
            case 7 -> field = null;
            case 8 -> field = FRIEDHOF;
            case 9 -> field = HEXENBAUM;
            case 10 -> field = STEINKREIS;
            default -> field = MITTE;
        }
        return field;
    }

    public int getNumber() {
        for (int i = 2; i < 11; i++) {
            if (getField(i) == this) {
                return i;
            }
        }
        return 0;
    }
}
