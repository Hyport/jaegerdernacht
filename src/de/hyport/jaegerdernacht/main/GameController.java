package de.hyport.jaegerdernacht.main;


import de.hyport.jaegerdernacht.roles.Role;
import de.hyport.jaegerdernacht.ui.InventoryBuilder;
import de.hyport.jaegerdernacht.ui.InventoryListener;
import de.hyport.jaegerdernacht.ui.TitleBuilder;
import org.bukkit.*;
import org.bukkit.boss.BarColor;
import org.bukkit.boss.BarStyle;
import org.bukkit.boss.BossBar;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.Scoreboard;

import javax.annotation.Nullable;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

public class GameController {

    private final Main plugin;
    private final LinkedList<Field> fields = new LinkedList<>();
    private final LinkedList<GamePlayer> players = new LinkedList<>();
    private boolean gameRunning;
    private int currentPlayerIndex = 0;
    // card stacks
    private CardStack greenStack;
    private CardStack redStack;
    private CardStack blueStack;
    private final CardStack greenDiscard;
    private final CardStack redDiscard;
    private final CardStack blueDiscard;

    private boolean firstRound;
    private BossBar bossBar;

    public static final AtomicReference<String> userAnswer = new AtomicReference<>("");    // DisplayName of clicked item/answer
    // dice atomic variables
    public static final AtomicReference<DiceReason> diceReason = new AtomicReference<>();           // reason for throwing the dice
    public static final AtomicBoolean diceFieldChangeReady = new AtomicBoolean(false);     // dice for field change can be thrown
    public static final AtomicInteger diceResult = new AtomicInteger(0);                   // the result on the dice
    public static final AtomicBoolean diceDone = new AtomicBoolean(true);                  // dice is done
    public static final Object diceFlag = new Object();

    public static Card drawCard = null;

    public GameController(Main plugin) {
        this.plugin = plugin;
        fields.add(Field.OXANA);
        fields.add(Field.QUELL);
        fields.add(Field.KAPELLE);
        fields.add(Field.MITTE);
        fields.add(Field.FRIEDHOF);
        fields.add(Field.HEXENBAUM);
        fields.add(Field.STEINKREIS);
        greenStack = new CardStack("green", "cards.json");
        redStack = new CardStack("red", "cards.json");
        blueStack = new CardStack("blue", "cards.json");
        greenDiscard = new CardStack("green");
        redDiscard = new CardStack("red");
        blueDiscard = new CardStack("blue");
        gameRunning = false;
        firstRound = true;
    }

    public void startGame(Collection<? extends Player> onlinePlayers, ArrayList<Role> roles) {
        Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
            for (Player p : onlinePlayers) {
                Role role = roles.get(0);
                new InventoryBuilder(p, InventoryListener.FARB_WAHL).createColorInventory(players).build(plugin);
                String response = waitForUserInput(p);
                GamePlayer gamePlayer = new GamePlayer(p, role, this, ChatColor.getByChar(response.substring(1,2)));
                role.setGamePlayer(gamePlayer);
                players.add(gamePlayer);
                roles.remove(0);
            }
            if (players.size() == 0) {
                System.err.println("Players needed to start the game.");
                return;
            }
            bossBar = Bukkit.createBossBar("", BarColor.WHITE, BarStyle.SOLID);
            for (Player p : Bukkit.getOnlinePlayers()) {
                bossBar.addPlayer(p);
            }
            gameRunning = true;
            Collections.shuffle(players);
            runScoreboard();
            fillGuessedPlayer();
            checkIfGameEnds();
            players.get(currentPlayerIndex).execute();
        });
    }


    private void fillGuessedPlayer() {
        for(int i = 0; i < players.size();i++) {
            for (GamePlayer player : players) {
                players.get(i).getGuesses().put(player, "leer");
            }
            players.get(i).getGuesses().remove(players.get(i));
        }
    }

    private void runScoreboard() {
        Scoreboard scoreboard = Bukkit.getScoreboardManager().getMainScoreboard();
        Objective obj = scoreboard.registerNewObjective("sch", "aden", ChatColor.RED.toString()+ChatColor.BOLD+"Schadenspunkte:");
        obj.setDisplaySlot(DisplaySlot.SIDEBAR);
        Bukkit.getScheduler().scheduleSyncRepeatingTask(plugin, () -> {
            for (GamePlayer gp : players) {
                Score score = obj.getScore(gp.getPlayer().getDisplayName());
                score.setScore(gp.getDamage());
                //gp.getPlayer().setFoodLevel(16);
            }
        }, 1, 10);
    }

    /**
     * Checks if the game ends and continue with the next player
     */
    public void nextPlayer() {
        checkIfGameEnds();
        if (gameRunning) {
            if (currentPlayerIndex + 1 < players.size()) {
                currentPlayerIndex++;
            } else {
                firstRound = false;
                currentPlayerIndex = 0;
            }
            if (players.get(currentPlayerIndex).isAlive()) {
                players.get(currentPlayerIndex).execute();
            } else {
                nextPlayer();
            }
        }
    }

    public void checkIfGameEnds() {
        if (gameRunning) {
            HashSet<GamePlayer> winners = new HashSet<>();
            for (GamePlayer gp : players) {
                if (gp.getRole().hasWon()) {
                    winners.add(gp);
                }
            }
            if (!winners.isEmpty()) {
                gameRunning = false;
                // end game
                Bukkit.getScheduler().runTask(plugin, () -> {
                    bossBar.removeAll();
                    Bukkit.broadcastMessage(ChatColor.GREEN + "DAS SPIEL IST VORBEI! GEWONNEN HABEN:");
                    for (GamePlayer winner : winners) {
                        Bukkit.broadcastMessage(winner.getPlayer().getDisplayName() + " (" + winner.getRole().getRoleColor() + winner.getRole().getName() + ChatColor.RESET + ")");
                    }
                    for (GamePlayer gp : players) {
                        gp.getPlayer().setGameMode(GameMode.SPECTATOR);
                        if (winners.contains(gp)) {
                            gp.getPlayer().sendTitle(ChatColor.GREEN + "GEWONNEN", "", 20, 60, 20);
                        } else {
                            gp.getPlayer().sendTitle(ChatColor.RED + "VERLOREN", "", 20, 60, 20);
                        }
                    }
                });
            }
        }
    }

    public boolean isFirstRound() {
        return firstRound;
    }

    public int rollBothDices(Player p, DiceReason reason) {
        synchronized (diceFlag) {
            diceReason.set(reason);
            diceResult.set(-1);
            p.getInventory().setItem(0, InventoryBuilder.createItem(Material.DIAMOND, "§bWürfel"));
            if (reason.equals(DiceReason.FIELD_CHANGE) && getGamePlayer(p).getInventory().containsCard("Magischer Kompass")) {
                Card card = getGamePlayer(p).getInventory().getCard("Magischer Kompass");
                p.getInventory().setItem(2, InventoryBuilder.getCorrectItem(card).getItem(card.getTitle(), card.getText()));
            }
            while (!diceDone.getAndSet(false)) {
                try {
                    Thread.sleep(100);
                } catch (InterruptedException ignored) {
                }
            }
            while (!diceFieldChangeReady.getAndSet(false)) {
                try {
                    Thread.sleep(100);
                } catch (InterruptedException ignored) {
                }
            }
            getGamePlayer(p).getRole().startFieldChange();
            if (diceResult.get() != 1) {
                Bukkit.getScheduler().runTask(plugin, () -> InventoryListener.handleDice(p, this));
                while (!GameController.diceDone.get()) {
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException ignored) {
                    }
                }
            }
            diceDone.set(true);
            int result = diceResult.getAndSet(-1);
            if (reason.equals(DiceReason.FIELD_CHANGE)) {
                initFieldChange(p, result);
            }
            return result;
        }
    }

    private void initFieldChange(Player p, int diceResult) {
        if (diceResult == 7) {
            chooseField(p, fields.stream().filter(f -> f != getGamePlayer(p).getField()).collect(Collectors.toCollection(LinkedList::new)));
        } else if (diceResult == 1) { // KOMPASS
            chooseField(p, fields.stream().filter(f -> f.getArea() != getGamePlayer(p).getField().getArea()).collect(Collectors.toCollection(LinkedList::new)));
        } else {
            if (Field.getField(diceResult) == getGamePlayer(p).getField()) {
                // number leads to current field -> throw dice again
                rollBothDices(p, DiceReason.FIELD_CHANGE);
            } else {
                getGamePlayer(p).teleportToField(diceResult);
            }
        }
    }

    public int rollSingleDice(GamePlayer gp, int maxValue, DiceReason reason, ChatColor color) {
        synchronized (diceFlag) {
            diceReason.set(reason);
            while (!diceDone.getAndSet(false)) {
                try {
                    Thread.sleep(100);
                } catch (InterruptedException ignored) {
                }
            }
            diceResult.set(-1);
            Player p = gp.getPlayer();
            int number = (int) (Math.random() * maxValue) + 1;
            new TitleBuilder(plugin, p).rollDiceAnimation(number, maxValue, "", color, 0);
            while (!diceDone.get()) {
                try {
                    Thread.sleep(100);
                } catch (InterruptedException ignored) {
                }
            }
            diceResult.set(-1);
            return number;
        }
    }

    @Nullable
    public GamePlayer chooseTarget(GamePlayer gamePlayer, String title, String question) {
        return chooseTargetFromList(gamePlayer.getPlayer(), getPlayersWithoutSelf(gamePlayer), title, question);
    }

    @Nullable
    public GamePlayer chooseTargetFromList(Player player, LinkedList<GamePlayer> list, String title, String question) {
        if(players.isEmpty()) return null;
        new InventoryBuilder(player, InventoryListener.SPIELER_WAHL).createPlayerInventory(list, title, question).build(plugin);
        String response = waitForUserInput(player);
        return getPlayerByName(response);
    }

    @Nullable
    public Card chooseCardFromInventory(Player player, CardInventory inventory) {
        ArrayList<Card> cards = inventory
                .stream()
                .filter(card -> card.getType().equalsIgnoreCase("Ausrüstung"))
                .collect(Collectors.toCollection(ArrayList::new));
        new InventoryBuilder(player, InventoryListener.KARTEN_WAHL).createCardInventory(cards).build(plugin);
        String response = waitForUserInput(player);
        return inventory.getCard(response);
    }

    /**
     * shows a player all cards of a given CardInventory
     */
    public void showInventory(Player player, CardInventory inventory) {
        ArrayList<Card> cards = inventory
                .stream()
                .filter(Objects::nonNull)
                .filter(card -> card.getType().equalsIgnoreCase("Ausrüstung"))
                .collect(Collectors.toCollection(ArrayList::new));
        new InventoryBuilder(player, "§dKarten").createCardInventory(cards).build(plugin);
    }

    public void chooseField(Player player, LinkedList<Field> fieldList) {
        new InventoryBuilder(player, "Wähle einen Ort aus:").createFieldInventory(fieldList, players).build(plugin);
        String response = waitForUserInput(player);
        if (getFieldWithColoredName(response) == getGamePlayer(player).getField()) {
            chooseField(player, fieldList);
        }   else {
            getGamePlayer(player).teleportToField(getFieldWithColoredName(response).getNumber());
        }
    }

    public boolean makeBoolChoice(Player player, String title, String question) {
        new InventoryBuilder(player, title).createBoolInventory(title, question).build(plugin);
        String response = waitForUserInput(player);
        return response.equalsIgnoreCase(ChatColor.GREEN+"Ja");
    }

    public String waitForUserInput(Player player) {
        userAnswer.set("");
        while(userAnswer.get().equals("")) {
        }
        UserAnswerHandler.increaseCloseInventoryTokens(player);
        String result = userAnswer.getAndSet("");
        Bukkit.getScheduler().runTask(plugin, player::closeInventory);
        return result;
    }



    public void handleDeath(Player p) {
        Bukkit.getScheduler().runTask(plugin, () -> p.setGameMode(GameMode.SPECTATOR));
    }

    /* GETTER */

    public GamePlayer getPlayerByName(String name) {
        for (GamePlayer gp : players) {
            if (gp.getPlayer().getDisplayName().equalsIgnoreCase(name+ChatColor.RESET)) {
                return gp;
            }
        }
        return null;
    }

    public BossBar getBossBar() {
        return bossBar;
    }

    public Main getPlugin() {
        return plugin;
    }

    public LinkedList<Field> getFields() {
        return fields;
    }

    public LinkedList<GamePlayer> getPlayers() {
        return players;
    }

    public LinkedList<GamePlayer> getPlayersWithoutSelf(GamePlayer self) {
        LinkedList<GamePlayer> list = new LinkedList<>(players);
        list.remove(self);
        return list;
    }

    public int getCurrentPlayerIndex() {
        return currentPlayerIndex;
    }

    public boolean isGameRunning() {
        return gameRunning;
    }

    public Field getField(String fieldName) {
        for(Field f : fields) {
            if(f.getName().equals(fieldName)) {
                return f;
            }
        }
        return null;
    }

    public Field getFieldWithColoredName(String fieldName) {
        for(Field f : fields) {
            String fName = f.getColor().toString()+f.getName();
            if(fName.equals(fieldName)) {
                return f;
            }
        }
        return null;
    }

    public GamePlayer getGamePlayer(Player p) {
        for (GamePlayer gp : players) {
            if (gp.getPlayer() == p) {
                return gp;
            }
        }
        return null;
    }

    /* Card Stacks */

    public CardStack getGreenStack() {
        if(greenStack.isEmpty()) {
            greenStack = new CardStack(greenDiscard);
        }
        return greenStack;
    }

    public CardStack getGreenDiscard() {
        return greenDiscard;
    }

    public CardStack getBlueStack() {
        if(blueStack.isEmpty()) {
            blueStack = new CardStack(blueDiscard);
        }
        return blueStack;
    }

    public CardStack getBlueDiscard() {
        return blueDiscard;
    }

    public CardStack getRedStack() {
        if(redStack.isEmpty()) {
            redStack = new CardStack(redDiscard);
        }
        return redStack;
    }

    public CardStack getRedDiscard() {
        return redDiscard;
    }

}
