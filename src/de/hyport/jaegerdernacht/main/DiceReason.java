package de.hyport.jaegerdernacht.main;

public enum DiceReason {
    NORMAL,
    ATTACK,
    HEAL,
    FIELD_CHANGE
}
