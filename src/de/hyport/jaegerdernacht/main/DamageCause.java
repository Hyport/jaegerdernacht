package de.hyport.jaegerdernacht.main;

public enum DamageCause {
    ATTACK,
    HEXENBAUM,
    BLUTMOND,
    RED_EVENT,
    BLUE_EVENT,
    SPECIAL
}
