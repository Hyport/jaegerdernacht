package de.hyport.jaegerdernacht.main;

import de.hyport.jaegerdernacht.roles.Vampire;
import de.hyport.jaegerdernacht.roles.Werewolf;
import de.hyport.jaegerdernacht.roles.vampires.UrufHan;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;

import java.util.LinkedList;

public record Card(String type, String title, String text, String color) {

    public String getType() {
        return type;
    }

    public String getTitle() {
        return title;
    }

    public String getText() {
        return text;
    }

    public String getColor() {
        return color;
    }

    public ChatColor getChatColor() {
        if (color.equals("red")) {
            return ChatColor.RED;
        } else if (color.equals("blue")) {
            return ChatColor.BLUE;
        }else if (color.equals("green")) {
            return ChatColor.GREEN;
        }
        return ChatColor.RESET;
    }

    public void executeCard(GamePlayer gp) {
        gp.getInventory().add(this);
        GameController controller = gp.getGameController();
        GamePlayer target;
        Card chosenCard;
        switch (title) {
            case "Segen":
                if(controller.makeBoolChoice(gp.getPlayer(), getTitle(), getText())) {
                    gp.decreaseDamage(2, DamageCause.BLUE_EVENT);
                }
                break;
            case "Heiliger Zorn":
                for (GamePlayer p : controller.getPlayers()) {
                    if (p != gp) {
                        p.increaseDamage(2, DamageCause.BLUE_EVENT, gp);
                    }
                }
                break;
            case "Blutmond":
                target = controller.chooseTargetFromList(gp.getPlayer(), controller.getPlayers(), title, text);
                if (target != null) {
                    target.setDamage(7, DamageCause.BLUTMOND, gp);
                }
                break;
            case "Enttarnung":
                if (gp.getRole() instanceof Werewolf || gp.getRole() instanceof Vampire && !(gp.getRole() instanceof UrufHan)) {
                    gp.getRole().reveal();
                }
                break;
            case "Fernheilung":
                LinkedList<GamePlayer> possibleTargets = new LinkedList<>(controller.getPlayers());
                possibleTargets.remove(gp);
                target = controller.chooseTargetFromList(gp.getPlayer(), possibleTargets, title, text);
                if (target != null) {
                    target.decreaseDamage(target.rollDiceSixer(DiceReason.HEAL), DamageCause.BLUE_EVENT);
                }
                break;
            case "Heilung":
                boolean choice = controller.makeBoolChoice(gp.getPlayer(), getTitle(), getText());
                if (choice && gp.getRole() instanceof Werewolf) {
                    gp.setDamage(0, DamageCause.BLUE_EVENT, gp);
                    gp.getRole().reveal();
                }
                break;
            case "Energieschub":
                choice = controller.makeBoolChoice(gp.getPlayer(), getTitle(), getText());
                char firstLetter = gp.getRole().getName().toCharArray()[0];
                if (choice
                        && (firstLetter == 'A' || firstLetter == 'E' || firstLetter == 'U')) {
                    gp.setDamage(0, DamageCause.BLUE_EVENT, gp);
                    gp.getRole().reveal();
                }
                break;
            case "Vampirfledermaus":
                target = controller.chooseTarget(gp, title, text);
                if (target != null) {
                    target.increaseDamage(2, DamageCause.RED_EVENT, gp);
                    gp.decreaseDamage(1, DamageCause.RED_EVENT);
                }
                break;
            case "Blutspinne":
                target = controller.chooseTarget(gp, title, text);
                if (target != null) {
                    target.increaseDamage(2, DamageCause.RED_EVENT, gp);
                    gp.increaseDamage(2, DamageCause.RED_EVENT, gp);
                }
                break;
            case "Explosion":
                Field field = Field.getField(gp.getGameController().rollBothDices(gp.getPlayer(), DiceReason.ATTACK));
                if (field != null) {
                    for (GamePlayer p : field.getPlayers(controller.getPlayers())) {
                        p.increaseDamage(3, DamageCause.RED_EVENT, gp);
                    }
                }
                break;
            case "Dunkles Ritual":
                choice = controller.makeBoolChoice(gp.getPlayer(), getTitle(), getText());
                if (choice && gp.getRole() instanceof Vampire) {
                    gp.setDamage(0, DamageCause.RED_EVENT, gp);
                    gp.getRole().reveal();
                }
                break;
            case "Hinterhalt":
                target = controller.chooseTarget(gp, title, text);
                if (target != null) {
                    if (gp.rollDiceSixer(DiceReason.NORMAL) < 5) {
                        target.increaseDamage(3, DamageCause.SPECIAL, gp);
                    } else {
                        gp.increaseDamage(3, DamageCause.SPECIAL, gp);
                    }
                }
                break;
            case "Fallgrube":
                if (gp.getInventory().containsEquipmentCard()) {
                    target = controller.chooseTarget(gp, title, text);
                    if (target != null) {
                        chosenCard = controller.chooseCardFromInventory(gp.getPlayer(), gp.getInventory());
                        target.getInventory().add(chosenCard);
                        gp.getInventory().remove(chosenCard);
                    }
                } else {
                    gp.increaseDamage(1, DamageCause.SPECIAL, gp);
                }
                break;
            case "Attacke":
                LinkedList<GamePlayer> targetList = gp.getPlayersWithEquipmentCards();
                if (targetList.size() > 0) {
                    target = controller.chooseTargetFromList(gp.getPlayer(), targetList, title, text);
                    if (target != null) {
                        chosenCard = controller.chooseCardFromInventory(gp.getPlayer(), target.getInventory());
                        if (chosenCard != null) {
                            Bukkit.broadcastMessage(target.getPlayer().getDisplayName() + " gibt " + chosenCard.getChatColor() + chosenCard.getTitle() + ChatColor.RESET + " an " + gp.getPlayer().getDisplayName() + "!");
                            target.getInventory().remove(chosenCard);
                            gp.getInventory().add(chosenCard);
                        }
                    }
                }
                break;
            default:
                // handled at another place in code or manually
        }
    }
}
