package de.hyport.jaegerdernacht.main;

import org.bukkit.entity.Player;

import java.util.HashMap;

public final class UserAnswerHandler {

    private static final HashMap<Player, Integer> closeInventoryTokens = new HashMap<>();

    public static synchronized void increaseCloseInventoryTokens(Player player) {
        if (!closeInventoryTokens.containsKey(player)) {
            closeInventoryTokens.put(player, 1);
        } else {
            closeInventoryTokens.put(player, closeInventoryTokens.get(player) + 1);
        }
    }

    public static synchronized void decreaseCloseInventoryTokens(Player player) {
        if (!closeInventoryTokens.containsKey(player)) return;
        int count = closeInventoryTokens.get(player) - 1;
        closeInventoryTokens.put(player, Math.max(count, 0));
    }

    public static synchronized boolean hasCloseInventoryTokens(Player player) {
        return closeInventoryTokens.containsKey(player) && closeInventoryTokens.get(player) > 0;
    }

}
