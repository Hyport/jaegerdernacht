package de.hyport.jaegerdernacht.main;

import de.hyport.jaegerdernacht.ui.CardAnimation;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Stack;

public class CardStack extends Stack<Card>{

    private final String color;


    public CardStack(String color, String jsonPath) {
        super();
        this.color = color;

        // parse JSON file which contains the cards
        JSONParser parser = new JSONParser();
        JSONObject baseObject;
        InputStream inputStream = getClass().getResourceAsStream(jsonPath);
        assert inputStream != null;
        try {
            baseObject = (JSONObject) parser.parse(new InputStreamReader(inputStream, StandardCharsets.UTF_8));
        } catch(ParseException | IOException e){
            e.printStackTrace();
            return;
        }
        JSONArray array = (JSONArray) baseObject.get(color);
        ArrayList<Card> cards = new ArrayList<>();
        for(Object o : array) {
            JSONObject jsonObject = (JSONObject) o;
            String type = (String) jsonObject.get("type");
            String title = (String) jsonObject.get("title");
            String text = (String) jsonObject.get("text");
            long count = (Long) jsonObject.get("count");
            for(int i = 0; i < count; i++) {
                cards.add(new Card(type, title, text, color));
            }
        }

        // push cards on stack
        Collections.shuffle(cards);
        for (Card c : cards) {
            push(c);
        }
    }

    // use this when discard files are shuffled and put back to main stack
    public CardStack(CardStack cardStack) {
        super();
        this.color = cardStack.color;
        Collections.shuffle(cardStack);
        for(Card c : cardStack) {
            this.push(c);
        }
        cardStack.clear();
    }

    // Ablage
    public CardStack(String color) {
        super();
        this.color = color;
    }

    public String getColor() {
        return color;
    }

    public Card drawCard(GamePlayer gamePlayer) {
        Card c = new CardAnimation(gamePlayer.getGameController().getPlugin()).playAnimation(gamePlayer, this);
        gamePlayer.getGameController().waitForUserInput(gamePlayer.getPlayer());
        return c;
    }

    public boolean isEmpty() {
        return size() == 0;
    }

}
