package de.hyport.jaegerdernacht.main;

import de.hyport.jaegerdernacht.roles.Role;
import de.hyport.jaegerdernacht.roles.Werewolf;
import de.hyport.jaegerdernacht.roles.events.AttackEvent;
import de.hyport.jaegerdernacht.roles.events.DeathEvent;
import de.hyport.jaegerdernacht.roles.events.GameEventListener;
import de.hyport.jaegerdernacht.roles.vampires.Valkyra;
import de.hyport.jaegerdernacht.roles.vampires.Wendur;
import de.hyport.jaegerdernacht.roles.werewolves.Gundwolf;
import de.hyport.jaegerdernacht.ui.ClickableItems;
import de.hyport.jaegerdernacht.ui.InventoryBuilder;
import de.hyport.jaegerdernacht.ui.ItemBuilder;
import de.hyport.jaegerdernacht.ui.TitleBuilder;
import de.hyport.jaegerdernacht.worldmanagement.BasicListener;
import org.bukkit.*;
import org.bukkit.boss.BossBar;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.stream.Collectors;

public class GamePlayer {

    private final Player player;
    private final Role role;
    private final GameController gameController;
    private final CardInventory inventory;
    private final ChatColor playerColor;

    private int damage;
    private boolean alive;
    private Field currentField;

    private final HashMap<GamePlayer, String> guesses = new HashMap<>();

    /* SETUP */

    public GamePlayer(Player player, Role role, GameController gameController, ChatColor playerColor) {
        this.player = player;
        this.role = role;
        this.damage = 0;
        this.gameController = gameController;
        this.playerColor = playerColor;
        this.setPersonalData();
        this.alive = true;
        this.inventory = new CardInventory(this);
        currentField = Field.MITTE;
        Bukkit.getScheduler().runTask(gameController.getPlugin(), () -> player.teleport(currentField.getLocation()));
    }

    private void setPersonalData() {
        Bukkit.getScheduler().runTask(gameController.getPlugin(), () -> {
            player.setDisplayName(playerColor+ BasicListener.getNickname(player.getName()) +ChatColor.RESET);
            player.setPlayerListName(player.getDisplayName());
            Scoreboard scoreboard = Bukkit.getScoreboardManager().getMainScoreboard();
            Team team = scoreboard.registerNewTeam(player.getName());
            team.setColor(playerColor);
            team.setPrefix(playerColor.toString());
            player.setScoreboard(scoreboard);
            ItemStack skull = new ItemStack(Material.PLAYER_HEAD);
            SkullMeta meta = (SkullMeta) skull.getItemMeta();
            assert meta != null;
            meta.setOwningPlayer(Bukkit.getOfflinePlayer(player.getUniqueId()));
            meta.setDisplayName(role.getRoleColor() + role.getName());
            skull.setItemMeta(meta);
            player.setGameMode(GameMode.ADVENTURE);
            player.setFoodLevel(16);
            //player.setHealthScale(role.getDeadPoint());
            //player.setHealth(role.getDeadPoint());
            player.getInventory().clear();
            player.getInventory().setItem(1, new ItemStack(Material.SPYGLASS));
            player.getInventory().setItem(3, InventoryBuilder.createItem(Material.REDSTONE, "§dKarten"));
            player.getInventory().setItem(5, new ItemBuilder(Material.ENDER_CHEST).setName("§bSpielerübersicht").build());
            player.getInventory().setItem(8, skull);
        });
    }

    public void execute() {
        BossBar bossBar = gameController.getBossBar();
        bossBar.setTitle(player.getDisplayName());
        bossBar.setProgress(0);
        inventory.removeEventCards();

        if (gameController.isFirstRound()) {
            Field.MITTE.drawGreenCard(this, false);
        }
        role.startRound();
        gameController.rollBothDices(getPlayer(), DiceReason.FIELD_CHANGE);
        bossBar.setProgress(0.4);
        currentField.startAction(this);
        gameController.checkIfGameEnds();
        bossBar.setProgress(0.6);
        role.startAttack();
        if (!gameController.isFirstRound()) {
            attack();
        }
        bossBar.setProgress(0.8);
        role.startEndRound();
        Bukkit.getScheduler().runTask(gameController.getPlugin(), () -> player.getInventory().setItem(6, ClickableItems.END_ROUND.getItem("WEITER", "")));
        bossBar.setProgress(1);
        gameController.waitForUserInput(player);
        role.endRound();
        gameController.checkIfGameEnds();
        boolean zeitsprung = inventory.containsCard("Zeitsprung");
        if(zeitsprung) {
            execute();
        } else if (role instanceof Wendur w && w.getAdditionalRounds() > 0) {
            Bukkit.broadcastMessage(getPlayer().getDisplayName() + " (" + role.getRoleColor() + role.getName() + ChatColor.RESET + ") ist noch einmal dran!");
            execute();
        } else {
            gameController.nextPlayer();
        }
    }

    /* ATTACK */

    public void attack() {
        if (!getAttackablePlayers().isEmpty()) {
            LinkedList<GamePlayer> attackablePlayers = getAttackablePlayers();
            StringBuilder attackablePlayersString = new StringBuilder("(");
            for (int i = 0; i < attackablePlayers.size()-1; i++) {
                attackablePlayersString
                        .append(attackablePlayers.get(i).getPlayer().getDisplayName())
                        .append(", ");
            }
            attackablePlayersString.append(attackablePlayers.get(attackablePlayers.size()-1).getPlayer().getDisplayName()).append(")");
            if (gameController.makeBoolChoice(player, "Spieler angreifen? " + attackablePlayersString, "Möchtest du jemanden angreifen? " + attackablePlayersString)) {
                GamePlayer target = getGameController().chooseTargetFromList(getPlayer(), attackablePlayers, "ANGRIFF", "Wen möchtest du angreifen?");
                if (target != null) {
                    Bukkit.broadcastMessage(player.getDisplayName() + " greift " + target.getPlayer().getDisplayName() + " an!");
                    int attackPoints = calculateAttackPoints();
                    if (inventory.containsCard("Feuerzauber")) {
                        Bukkit.broadcastMessage(player.getDisplayName() + " verwendet den " + ChatColor.RED + "Feuerzauber!");
                        for (Field f : gameController.getFields()) {
                            if (f.getArea() == target.getField().getArea()) {
                                for (GamePlayer p : f.getPlayers(gameController.getPlayers())) {
                                    if (p != this) {
                                        executeAttackOnPlayer(attackPoints, p);
                                    }
                                }
                            }
                        }
                    } else {
                        executeAttackOnPlayer(attackPoints, target);
                    }
                }
            }
        }
    }

    public int calculateAttackPoints() {
        int attackPoints;
        if(role instanceof Valkyra && role.getAbility().isEnabled() && role.isRevealed()) {
            Bukkit.broadcastMessage(player.getDisplayName() + " benutzt seine/ihre Fähigkeit als " + role.getRoleColor() + role.getName());
            attackPoints = rollDiceFourer(DiceReason.ATTACK);
        } else if (inventory.containsCard("Fackel")) {
            Bukkit.broadcastMessage(player.getDisplayName() + " verwendet die " + ChatColor.RED + "Fackel" + ChatColor.RESET + "!");
            attackPoints = rollDiceFourer(DiceReason.ATTACK);
        } else {
            attackPoints = Math.abs(rollDiceSixer(DiceReason.ATTACK)-rollDiceFourer(DiceReason.ATTACK));
        }
        if (attackPoints > 0) {
            attackPoints += getInventory().getEisenfaustCount();
            if (inventory.containsCard("Knochenlanze")) {
                Card knochenlanze = inventory.getCard("Knochenlanze");
                if (role instanceof Werewolf) {
                    if (gameController.makeBoolChoice(player, knochenlanze.getTitle(), knochenlanze.getText())) {
                        Bukkit.broadcastMessage(player.getDisplayName() + " setzt die " + knochenlanze.getChatColor() + knochenlanze.getTitle() +  ChatColor.RESET + " ein!");
                        role.reveal();
                        attackPoints += 2;
                    }
                }
            }
        }
        return attackPoints;
    }

    public void executeAttackOnPlayer(int attackPoints, GamePlayer target) {
        if ((inventory.containsCard("Schutzumhang") || target.getInventory().containsCard("Schutzumhang")) && attackPoints > 0) {
            Bukkit.broadcastMessage("Der " + ChatColor.BLUE + "Schutzumhang " + ChatColor.RESET +  " reduziert den Schaden um "+ ChatColor.DARK_GREEN +"1" +ChatColor.RESET+"!");
            attackPoints--;
        }
        target.increaseDamage(attackPoints, DamageCause.ATTACK, this);

        // message roles
        if (target.getRole() instanceof GameEventListener) {
            ((GameEventListener) target.getRole()).receiveAttack(new AttackEvent(this, target, attackPoints)); // for Volko and Walburga
        }
        if (role instanceof GameEventListener) {
            ((GameEventListener) role).receiveAttack(new AttackEvent(this, target, attackPoints));
        }
    }

    private LinkedList<GamePlayer> getAttackablePlayers() {
        LinkedList<GamePlayer> targetList = new LinkedList<>();
        if (getInventory().containsCard("Armbrust")) {
            for (Field f : gameController.getFields()) {
                if (f.getArea() != currentField.getArea()) {
                    targetList.addAll(f.getPlayers(gameController.getPlayers()
                            .stream()
                            .filter(gp -> gp != this)
                            .collect(Collectors.toCollection(LinkedList::new))));
                }
            }
        } else {
            for (Field f : gameController.getFields()) {
                if (f.getArea() == currentField.getArea()) {
                    targetList.addAll(f.getPlayers(gameController.getPlayers()
                            .stream()
                            .filter(gp -> gp != this)
                            .collect(Collectors.toCollection(LinkedList::new))));
                }
            }
        }
        return targetList;
    }

    /* DAMAGE */

    public void increaseDamage(int dmg, DamageCause cause, GamePlayer source) {
        if (role instanceof Gundwolf) {
            if(((Gundwolf) role).isAbilityActive()) {
                dmg = 0;
            }
        }
        if (cause == DamageCause.HEXENBAUM && inventory.containsCard("Schutzamulett")) {
            Bukkit.broadcastMessage(player.getDisplayName() + " ist geschützt durch " + ChatColor.BLUE + "Schutzamulett" + ChatColor.RESET + "!");
            dmg = 0;
        }
        if (cause == DamageCause.ATTACK && inventory.containsCard("Schützende Aura")) {
            Bukkit.broadcastMessage(player.getDisplayName() + " ist geschützt durch " + ChatColor.BLUE + "Schützende Aura" + ChatColor.RESET +"!");
            dmg = 0;
        }
        if (cause == DamageCause.RED_EVENT && inventory.containsCard("Talisman")) {
            Bukkit.broadcastMessage(player.getDisplayName() + " ist geschützt durch " + ChatColor.BLUE + "Talisman" + ChatColor.RESET +"!");
            dmg = 0;
        }
        if (dmg > 0) {
            int delay = 0;
            for (int i = 0; i < dmg; i++) {
                Bukkit.getScheduler().runTaskLater(gameController.getPlugin(), () -> player.getWorld().strikeLightningEffect(player.getLocation()), delay);
                delay += 15;
            }
        }
        if (source != this) {
            Bukkit.broadcastMessage(player.getDisplayName() + " bekommt durch " + source.getPlayer().getDisplayName() + " §4" + dmg + " §rSchadenspunkte.");
        } else {
            Bukkit.broadcastMessage(player.getDisplayName() + " bekommt §4" + dmg + " §rSchadenspunkte.");
        }
        damage += dmg;
        if (damage >= role.getDeadPoint()) {
            die(source);
        } else {
            //Bukkit.getScheduler().runTask(gameController.getPlugin(), () -> player.setHealth(role.getDeadPoint()-damage));
        }
    }

    public void decreaseDamage(int dmg, DamageCause cause) {
        Bukkit.broadcastMessage(player.getDisplayName() + " wird um §2" + (damage-dmg > 0 ? dmg : damage) + " §rSchadenspunkte geheilt.");
        damage -= dmg;
        if(damage < 0) damage = 0;
        //Bukkit.getScheduler().runTask(gameController.getPlugin(), () -> player.setHealth(role.getDeadPoint()-damage));
    }

    public void setDamage(int dmg, DamageCause cause, GamePlayer source) {
        if (cause == DamageCause.BLUTMOND && inventory.containsCard("Schutzring")) {
            dmg = damage;
        }
        if (dmg > damage) {
            increaseDamage(dmg-damage, cause, source);
        } else if (dmg < damage) {
            decreaseDamage(damage-dmg, cause);
        }
    }

    public void die(GamePlayer source) {
        alive = false;
        role.reveal();
        Bukkit.broadcastMessage(player.getDisplayName() + " ist aus dem Spiel ausgeschieden!");
        gameController.checkIfGameEnds();
        if (source != this) {
            if (source.getInventory().containsCard("Rucksack") && inventory.containsEquipmentCard()) {
                Bukkit.broadcastMessage(source.getPlayer().getDisplayName() + " bekommt alle Ausrüstungskarten von " + player.getDisplayName());
                source.getInventory().addAll(inventory);
                inventory.clear();
            } else if (inventory.containsEquipmentCard()){
                Card chosenCard = getGameController().chooseCardFromInventory(source.player, inventory);
                source.getInventory().add(chosenCard);
                getInventory().remove(chosenCard);
            }
        }
        for (GamePlayer p : getGameController().getPlayers()) {
            if (p.getRole() instanceof GameEventListener) {
                ((GameEventListener) p.getRole()).receiveDeath(new DeathEvent(this));
            }
        }
        currentField = Field.MITTE;
        Bukkit.getScheduler().runTask(gameController.getPlugin(), () -> getPlayer().teleport(currentField.getLocation()));
        getGameController().handleDeath(getPlayer());
    }

    /* REST */

    public int rollDiceSixer(DiceReason reason) {
        ChatColor color;
        if (reason == DiceReason.ATTACK) {
            color = ChatColor.DARK_RED;
        } else {
            color = ChatColor.DARK_BLUE;
        }
        return gameController.rollSingleDice(this, 6, reason, color);
    }

    public int rollDiceFourer(DiceReason reason) {
        ChatColor color;
        if (reason == DiceReason.ATTACK) {
            color = ChatColor.RED;
        } else {
            color = ChatColor.BLUE;
        }
        return gameController.rollSingleDice(this, 4, reason, color);
    }

    public void teleportToField(int number) {
        currentField = Field.getField(number);
        Bukkit.getScheduler().runTask(gameController.getPlugin(), () -> player.teleport(currentField.getLocation()));
        new TitleBuilder(gameController.getPlugin(), player).sendDelayedTitle(0, 60, currentField.getColor()+currentField.getName(), "", Sound.ENTITY_ENDERMAN_TELEPORT);
    }

    public LinkedList<GamePlayer> getPlayersWithEquipmentCards() {
        return getGameController().getPlayers()
                .stream()
                .filter(p -> p.getInventory().containsEquipmentCard() && p != this)
                .filter(GamePlayer::isAlive)
                .collect(Collectors.toCollection(LinkedList::new));
    }

    /* GETTER */

    public String getPlayerColor() {
        return playerColor.toString();
    }

    public Player getPlayer() {
        return player;
    }

    public Role getRole() {
        return role;
    }

    public int getDamage() {
        return damage;
    }

    public GameController getGameController() {
        return gameController;
    }

    public CardInventory getInventory() {
        return inventory;
    }

    public boolean isAlive() {
        return alive;
    }

    public Field getField() {
        return currentField;
    }

    public HashMap<GamePlayer, String> getGuesses() {
        return guesses;
    }
}


