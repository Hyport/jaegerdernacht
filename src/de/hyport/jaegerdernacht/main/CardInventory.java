package de.hyport.jaegerdernacht.main;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;

import java.util.ArrayList;

public class CardInventory extends ArrayList<Card> {

    private final GamePlayer owner;

    public CardInventory(GamePlayer owner) {
        this.owner = owner;
    }

    public boolean containsCard(String title) {
        for (Card c : this) {
            if (c != null && c.getTitle().equalsIgnoreCase(title)) {
                return true;
            }
        }
        return false;
    }

    private boolean containsEventCard() {
        for (Card c : this) {
            if (c != null && c.getType().equalsIgnoreCase("Ereignis")) {
                return true;
            }
        }
        return false;
    }

    public boolean containsEquipmentCard() {
        for (Card c : this) {
            if (c != null && c.getType().equalsIgnoreCase("Ausrüstung")) {
                return true;
            }
        }
        return false;
    }

    public Card getCard(String title) {
        for (Card c : this) {
            if (c != null && title.contains(c.getTitle())) {
                return c;
            }
        }
        return null;
    }

    public Card getEventCard() {
        for (Card c : this) {
            if (c.getType().equalsIgnoreCase("Ereignis")) {
                return c;
            }
        }
        return null;
    }

    public boolean removeCard(String title) {
        if (containsCard(title)) {
            return this.remove(getCard(title));
        }
        return false;
    }

    public void removeEventCards() {
        while (containsEventCard()) {
            Card c = getEventCard();
            remove(c);
            if (c.getColor().equalsIgnoreCase("red")) {
                owner.getGameController().getRedDiscard().push(c);
            } else if(c.getColor().equalsIgnoreCase("blue")) {
                owner.getGameController().getBlueDiscard().push(c);
            }
        }
    }

    public int getEisenfaustCount() {
        int count = 0;
        // count Eisenfaust
        for (Card c : this) {
            if (c.getTitle().equalsIgnoreCase("Eisenfaust")) {
                count++;
            }
        }
        // output impact
        if (count > 0) {
            Bukkit.broadcastMessage(owner.getPlayer().getDisplayName() + " verwendet " + count  + ChatColor.RESET + "-mal die Karte "+ChatColor.RED+"Eisenfaust!");
        }
        return count;
    }

}
