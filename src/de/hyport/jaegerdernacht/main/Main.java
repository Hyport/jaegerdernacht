package de.hyport.jaegerdernacht.main;

import de.hyport.jaegerdernacht.roles.Human;
import de.hyport.jaegerdernacht.roles.Role;
import de.hyport.jaegerdernacht.roles.Vampire;
import de.hyport.jaegerdernacht.roles.Werewolf;
import de.hyport.jaegerdernacht.roles.humans.Adda;
import de.hyport.jaegerdernacht.roles.humans.Bella;
import de.hyport.jaegerdernacht.roles.humans.Chloe;
import de.hyport.jaegerdernacht.roles.humans.Daniel;
import de.hyport.jaegerdernacht.roles.vampires.*;
import de.hyport.jaegerdernacht.roles.werewolves.*;
import de.hyport.jaegerdernacht.ui.InventoryBuilder;
import de.hyport.jaegerdernacht.ui.InventoryListener;
import de.hyport.jaegerdernacht.worldmanagement.BasicListener;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitTask;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Team;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.Collections;


public class Main extends JavaPlugin implements CommandExecutor {

    private GameController gameThread;

    @Override
    public void onEnable() {
        gameThread = new GameController(this);
        Bukkit.getPluginManager().registerEvents(new InventoryListener(gameThread), this);
        Bukkit.getPluginManager().registerEvents(new BasicListener(gameThread), this);
    }

    @Override
    public void onDisable() {
        for (BukkitTask task : Bukkit.getScheduler().getPendingTasks()) {
            task.cancel();
        }
        for (Team t : Bukkit.getScoreboardManager().getMainScoreboard().getTeams()) {
            t.unregister();
        }
        for (Objective o : Bukkit.getScoreboardManager().getMainScoreboard().getObjectives()) {
            o.unregister();
        }
        if (gameThread.getBossBar() != null)
            gameThread.getBossBar().removeAll();
    }

    @Override
    public boolean onCommand(@Nonnull CommandSender sender, Command command, @Nonnull String label, @Nonnull String[] args) {
        String cmd = command.getName().toLowerCase();
        if (cmd.equals("start")) {
            if (gameThread.isGameRunning()) {
                Bukkit.broadcastMessage("Spiel läuft bereits.");
            } else {
                Bukkit.broadcastMessage("Jäger der Nacht gestartet.");
                gameThread.startGame(Bukkit.getOnlinePlayers(), getRoles(Bukkit.getOnlinePlayers().size()));
            }
        }
        else if (cmd.equals("test") && sender instanceof Player) {
            new InventoryBuilder(((Player) sender).getPlayer(), "Spieler").createPlayerInventar(gameThread, gameThread.getGamePlayer(((Player) sender).getPlayer())).build(this);
        } else if(cmd.equals("blue") && sender instanceof Player p) {
            for (Card c : gameThread.getBlueDiscard()) {
                p.sendMessage(c.getTitle());
            }
        } else if(cmd.equals("red") && sender instanceof Player p) {
            for (Card c : gameThread.getRedDiscard()) {
                p.sendMessage(c.getTitle());
            }
        } else if (cmd.equals("next") && sender instanceof Player p && p.getName().equals("BenLuno")) {
            Bukkit.getScheduler().runTaskAsynchronously(this, () -> gameThread.nextPlayer());
        } else if (cmd.equals("repair")) {
            GameController.userAnswer.set("");
        }
        return false;
    }

    private ArrayList<Role> getRoles(int playerNumber) {
        ArrayList<Role> roles = new ArrayList<>();
        ArrayList<Vampire> vampires = getVampireList();
        ArrayList<Werewolf> werewolves = getWerwolfList();
        ArrayList<Human> humans = getHumanList();
        // base
        roles.add(vampires.get(0));
        roles.add(vampires.get(1));
        roles.add(werewolves.get(0));
        roles.add(werewolves.get(1));

        // if (playerNumber < 4) return; // commented for test purposes
        switch (playerNumber) {
            case 6:
                roles.add(humans.get(1));
            case 5:
                roles.add(humans.get(0));
                break;
            case 8:
                roles.add(humans.get(1));
            case 7:
                roles.add(vampires.get(2));
                roles.add(werewolves.get(2));
                roles.add(humans.get(0));
        }
        Collections.shuffle(roles);
        return roles;
    }

    private ArrayList<Vampire> getVampireList() {
        ArrayList<Vampire> vampires = new ArrayList<>();
        if (getRandBool())
            vampires.add(new Uriel());
        else
            vampires.add(new UrufHan());
        if (getRandBool())
            vampires.add(new Valkyra());
        else
            vampires.add(new Volko());
        if (getRandBool())
            vampires.add(new Walburga());
        else
            vampires.add(new Wendur());
        Collections.shuffle(vampires);
        return vampires;
    }

    private ArrayList<Werewolf> getWerwolfList() {
        ArrayList<Werewolf> werewolves = new ArrayList<>();
        werewolves.add(new Etta());
        if (getRandBool())
            werewolves.add(new Flicka());
        else
            werewolves.add(new Frederik());
        if (getRandBool())
            werewolves.add(new Gregor());
        else
            werewolves.add(new Gundwolf());
        Collections.shuffle(werewolves);
        return werewolves;
    }

    private ArrayList<Human> getHumanList() {
        ArrayList<Human> humans = new ArrayList<>();
        humans.add(new Adda());
        humans.add(new Bella());
        humans.add(new Chloe());
        humans.add(new Daniel());
        Collections.shuffle(humans);
        return humans;
    }

    private boolean getRandBool() {
        return Math.random()*2 < 0.5;
    }
}