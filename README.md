# JÄGER DER NACHT #

Hier wird das Minecraft-Plugin zu dem Spiel "Jäger der Nacht" beschrieben.  
Minecraft-Version: 1.20.2

## Grundaufbau ##

Der Hauptordner unterteilt sich in 3 Unterordner.

### main ###

* Karten und Kartenstapel, die Karten finden sich in der cards.json
* Spieler und Schadensverwaltung
* Spielfelder
* Spielablauf

<b>Achtung</b>: Die Interaktion mit dem UI erfolgt möglichst zentralisiert über die Klasse GameController!

### roles ###

Dieses Package enthält die verschiedenen Rollen mit jeweiligen Zielen und Teams.

Wichtig: Implementiere bei jeder neuen Rolle eine Ability!

### ui ###

Dient der Interaktion mit dem Minecraft-Spieler

### worldmanagement ###

Beinhaltet Basisregeln für die Minecraft-Welt